# Lombok with Eclipse Intergration(@author:Abhinit Kumar)

Lombok is a java library that automatically plugs into your editor and build tools, spicing up your java.
Never write another getter or equals method again, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.


## Configuration ##
- Exit Eclipse(if it is open) and downloaded jar from [https://projectlombok.org/download](https://projectlombok.org/download)

- execute command: java -jar lombok.jar

- This command will open window as shown here [https://projectlombok.org/setup/eclipse](https://projectlombok.org/setup/eclipse), install and quit the installer.

- Add jar to build path/add it to pom.xml.

- restart eclipse.

- Go to Eclipse --> About Eclipse --> check 'Lombok v1.16.18 "Dancing Elephant" is installed.[https://projectlombok.org/](https://projectlombok.org/')

- To enable Lombok for the project: Enable annotation processing in the respective IDE. That's it. It worked. I did not change eclipse init  script.

```
Note: Read the note in following image regarding -vm options If you start Eclipse with a custom -vm parameter, 
you'll need to add: -vmargs -javaagent:<path-to-lombok-jar>/lombok.jar
```
