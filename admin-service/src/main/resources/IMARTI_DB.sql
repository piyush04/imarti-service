-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: imarti
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gn_address`
--

DROP TABLE IF EXISTS `gn_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_address` (
  `adrs_id` varchar(60) NOT NULL,
  `adrs_user_id` varchar(60) NOT NULL,
  `adrs_country_id` varchar(60) NOT NULL,
  `adrs_state_id` varchar(60) NOT NULL,
  `adrs_city_id` varchar(60) NOT NULL,
  `adrs_address_line_1` varchar(60) NOT NULL,
  `adrs_address_line_2` varchar(60) DEFAULT NULL,
  `adrs_address_line_3` varchar(60) DEFAULT NULL,
  `adrs_pin_code` varchar(10) NOT NULL,
  `adrs_last_updated` datetime DEFAULT NULL,
  `adrs_last_updated_by` varchar(45) DEFAULT NULL,
  `adrs_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`adrs_id`),
  UNIQUE KEY `adrs_id_UNIQUE` (`adrs_id`),
  KEY `fk_addr_user_id_idx` (`adrs_user_id`),
  KEY `fk_addr_state_id_idx` (`adrs_state_id`),
  KEY `fk_addr_city_id_idx` (`adrs_city_id`),
  KEY `fk_addr_country_id_idx` (`adrs_country_id`),
  CONSTRAINT `fk_adrs_city_id` FOREIGN KEY (`adrs_city_id`) REFERENCES `gn_city` (`city_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_adrs_country_id` FOREIGN KEY (`adrs_country_id`) REFERENCES `gn_country` (`ctry_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_adrs_state_id` FOREIGN KEY (`adrs_state_id`) REFERENCES `gn_state` (`stae_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_adrs_user_id` FOREIGN KEY (`adrs_user_id`) REFERENCES `gn_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_address`
--

LOCK TABLES `gn_address` WRITE;
/*!40000 ALTER TABLE `gn_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_city`
--

DROP TABLE IF EXISTS `gn_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_city` (
  `city_id` varchar(60) NOT NULL,
  `city_state_id` varchar(60) NOT NULL,
  `city_code` varchar(60) NOT NULL,
  `city_name` varchar(60) NOT NULL,
  `city_last_updated` datetime DEFAULT NULL,
  `city_last_updated_by` varchar(45) DEFAULT NULL,
  `city_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city_id_UNIQUE` (`city_id`),
  KEY `fk_city_state_id_idx` (`city_state_id`),
  CONSTRAINT `fk_city_state_id` FOREIGN KEY (`city_state_id`) REFERENCES `gn_state` (`stae_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_city`
--

LOCK TABLES `gn_city` WRITE;
/*!40000 ALTER TABLE `gn_city` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_code`
--

DROP TABLE IF EXISTS `gn_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_code` (
  `code_id` varchar(60) NOT NULL,
  `code_type_id` varchar(60) DEFAULT NULL,
  `code_code` varchar(8) NOT NULL,
  `code_short_description` varchar(50) DEFAULT NULL,
  `code_description` varchar(255) DEFAULT NULL,
  `code_display` varchar(1) NOT NULL,
  `code_last_updated` datetime DEFAULT NULL,
  `code_last_updated_by` varchar(45) DEFAULT NULL,
  `code_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `code_id_UNIQUE` (`code_id`),
  KEY `fk_code_type_id_idx` (`code_type_id`),
  CONSTRAINT `fk_code_type_id` FOREIGN KEY (`code_type_id`) REFERENCES `gn_code_type` (`cdty_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_code`
--

LOCK TABLES `gn_code` WRITE;
/*!40000 ALTER TABLE `gn_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_code_type`
--

DROP TABLE IF EXISTS `gn_code_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_code_type` (
  `cdty_id` varchar(60) NOT NULL,
  `cdty_code` varchar(4) NOT NULL,
  `cdty_description` varchar(255) DEFAULT NULL,
  `cdty_last_updated` datetime DEFAULT NULL,
  `cdty_last_updated_by` varchar(45) DEFAULT NULL,
  `cdty_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`cdty_id`),
  UNIQUE KEY `cdty_id_UNIQUE` (`cdty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_code_type`
--

LOCK TABLES `gn_code_type` WRITE;
/*!40000 ALTER TABLE `gn_code_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_code_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_country`
--

DROP TABLE IF EXISTS `gn_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_country` (
  `ctry_id` varchar(60) NOT NULL,
  `ctry_code` varchar(4) NOT NULL,
  `ctry_name` varchar(45) NOT NULL,
  `ctry_last_updated` datetime DEFAULT NULL,
  `ctry_last_updated_by` varchar(45) DEFAULT NULL,
  `ctry_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`ctry_id`),
  UNIQUE KEY `ctry_id_UNIQUE` (`ctry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_country`
--

LOCK TABLES `gn_country` WRITE;
/*!40000 ALTER TABLE `gn_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_menu`
--

DROP TABLE IF EXISTS `gn_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_menu` (
  `menu_id` varchar(60) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `menu_id_UNIQUE` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_menu`
--

LOCK TABLES `gn_menu` WRITE;
/*!40000 ALTER TABLE `gn_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_orders`
--

DROP TABLE IF EXISTS `gn_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_orders` (
  `ordr_id` varchar(60) NOT NULL,
  PRIMARY KEY (`ordr_id`),
  UNIQUE KEY `ordr_id_UNIQUE` (`ordr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_orders`
--

LOCK TABLES `gn_orders` WRITE;
/*!40000 ALTER TABLE `gn_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_routine`
--

DROP TABLE IF EXISTS `gn_routine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_routine` (
  `rotn_id` varchar(60) NOT NULL,
  PRIMARY KEY (`rotn_id`),
  UNIQUE KEY `rotn_id_UNIQUE` (`rotn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_routine`
--

LOCK TABLES `gn_routine` WRITE;
/*!40000 ALTER TABLE `gn_routine` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_routine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_shift`
--

DROP TABLE IF EXISTS `gn_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_shift` (
  `shft_id` varchar(60) NOT NULL,
  PRIMARY KEY (`shft_id`),
  UNIQUE KEY `shft_id_UNIQUE` (`shft_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_shift`
--

LOCK TABLES `gn_shift` WRITE;
/*!40000 ALTER TABLE `gn_shift` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_shift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_state`
--

DROP TABLE IF EXISTS `gn_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_state` (
  `stae_id` varchar(60) NOT NULL,
  `stae_country_id` varchar(60) NOT NULL,
  `stae_code` varchar(4) NOT NULL,
  `stae_name` varchar(45) NOT NULL,
  `stae_last_updated` datetime DEFAULT NULL,
  `stae_last_updated_by` varchar(45) DEFAULT NULL,
  `stae_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`stae_id`),
  UNIQUE KEY `stae_id_UNIQUE` (`stae_id`),
  KEY `fk_stae_country_id_idx` (`stae_country_id`),
  CONSTRAINT `fk_stae_country_id` FOREIGN KEY (`stae_country_id`) REFERENCES `gn_country` (`ctry_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_state`
--

LOCK TABLES `gn_state` WRITE;
/*!40000 ALTER TABLE `gn_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_user`
--

DROP TABLE IF EXISTS `gn_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_user` (
  `user_id` varchar(60) NOT NULL,
  `user_status_code_id` varchar(60) NOT NULL,
  `user_title_code_id` varchar(60) DEFAULT NULL,
  `user_login` varchar(45) NOT NULL,
  `user_pin_id` varchar(60) NOT NULL,
  `user_frst_name` varchar(45) NOT NULL,
  `user_middle_name` varchar(45) DEFAULT NULL,
  `user_last_name` varchar(45) DEFAULT NULL,
  `user_date_of_birth` varchar(45) DEFAULT NULL,
  `user-mobile_number` varchar(25) NOT NULL,
  `user_email` varchar(45) DEFAULT NULL,
  `user_last_updated` datetime DEFAULT NULL,
  `user_last_updated_by` varchar(45) DEFAULT NULL,
  `user_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `user_login_UNIQUE` (`user_login`),
  KEY `fk_user_status_code_id_idx` (`user_status_code_id`),
  KEY `fk_user_title_code_id_idx` (`user_title_code_id`),
  KEY `fk_user_pin_id_idx` (`user_pin_id`),
  CONSTRAINT `fk_user_pin_id` FOREIGN KEY (`user_pin_id`) REFERENCES `gn_user_pin` (`upin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_status_code_id` FOREIGN KEY (`user_status_code_id`) REFERENCES `gn_code` (`code_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_title_code_id` FOREIGN KEY (`user_title_code_id`) REFERENCES `gn_code` (`code_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_user`
--

LOCK TABLES `gn_user` WRITE;
/*!40000 ALTER TABLE `gn_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_user_pin`
--

DROP TABLE IF EXISTS `gn_user_pin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_user_pin` (
  `upin_id` varchar(60) NOT NULL,
  `upin_user_id` varchar(60) DEFAULT NULL,
  `upin_status_code_id` varchar(60) DEFAULT NULL,
  `upin_type_code_id` varchar(60) NOT NULL,
  `upin_pin_number` varchar(512) NOT NULL,
  `upin_effective_date` datetime NOT NULL,
  `upin_expiry_date` datetime DEFAULT NULL,
  `upin_last_login_date` datetime DEFAULT NULL,
  `upin_registration_number` varchar(45) DEFAULT NULL,
  `upin_login_count` int(11) DEFAULT NULL,
  `upin_salt` varchar(256) DEFAULT NULL,
  `upin_creation_date` datetime DEFAULT NULL,
  `upin_recipe` varchar(45) DEFAULT NULL,
  `upin_logout_period_end` datetime DEFAULT NULL,
  `upin_last_updated` datetime DEFAULT NULL,
  `upin_last_updated_by` varchar(45) DEFAULT NULL,
  `upin_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`upin_id`),
  UNIQUE KEY `upin_id_UNIQUE` (`upin_id`),
  KEY `fk_upin_user_id_idx` (`upin_user_id`),
  KEY `fk_upin_status_code_id_idx` (`upin_status_code_id`),
  KEY `fk_upin_type_code_id_idx` (`upin_type_code_id`),
  CONSTRAINT `fk_upin_status_code_id` FOREIGN KEY (`upin_status_code_id`) REFERENCES `gn_code` (`code_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_upin_type_code_id` FOREIGN KEY (`upin_type_code_id`) REFERENCES `gn_code` (`code_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_upin_user_id` FOREIGN KEY (`upin_user_id`) REFERENCES `gn_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_user_pin`
--

LOCK TABLES `gn_user_pin` WRITE;
/*!40000 ALTER TABLE `gn_user_pin` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_user_pin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gn_user_pin_history`
--

DROP TABLE IF EXISTS `gn_user_pin_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gn_user_pin_history` (
  `upht_id` varchar(60) NOT NULL,
  `upht_user_pin_id` varchar(60) DEFAULT NULL,
  `upht_pin_number` varchar(512) DEFAULT NULL,
  `upht_salt` varchar(256) DEFAULT NULL,
  `upht_recipe` varchar(45) DEFAULT NULL,
  `upht_registration_number` varchar(45) DEFAULT NULL,
  `upht_creation_date` datetime DEFAULT NULL,
  `upht_last_updated` datetime DEFAULT NULL,
  `upht_last_updated_by` varchar(45) DEFAULT NULL,
  `upht_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`upht_id`),
  UNIQUE KEY `upht_id_UNIQUE` (`upht_id`),
  KEY `fk_upht_user_pin_id_idx` (`upht_user_pin_id`),
  CONSTRAINT `fk_upht_user_pin_id` FOREIGN KEY (`upht_user_pin_id`) REFERENCES `gn_user_pin` (`upin_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gn_user_pin_history`
--

LOCK TABLES `gn_user_pin_history` WRITE;
/*!40000 ALTER TABLE `gn_user_pin_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_user_pin_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pymt_discount`
--

DROP TABLE IF EXISTS `pymt_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pymt_discount` (
  `dcnt_id` varchar(60) NOT NULL,
  `dcnt_status_code_id` varchar(60) DEFAULT NULL,
  `dcnt_code` varchar(8) DEFAULT NULL,
  `dcnt_start_date` datetime DEFAULT NULL,
  `dcnt_end_date` datetime DEFAULT NULL,
  `dcnt_descripton` varchar(255) DEFAULT NULL,
  `dcnt_last_updated` datetime DEFAULT NULL,
  `dcnt_last_updated_by` varchar(45) DEFAULT NULL,
  `dcnt_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`dcnt_id`),
  UNIQUE KEY `dcnt_id_UNIQUE` (`dcnt_id`),
  KEY `fk_dcnt_status_code_id_idx` (`dcnt_status_code_id`),
  CONSTRAINT `fk_dcnt_status_code_id` FOREIGN KEY (`dcnt_status_code_id`) REFERENCES `gn_code` (`code_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pymt_discount`
--

LOCK TABLES `pymt_discount` WRITE;
/*!40000 ALTER TABLE `pymt_discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `pymt_discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pymt_payment`
--

DROP TABLE IF EXISTS `pymt_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pymt_payment` (
  `paym_id` varchar(60) NOT NULL,
  `paym_user_id` varchar(60) DEFAULT NULL,
  `paym_type_code_id` varchar(60) DEFAULT NULL,
  `paym_discount_id` varchar(60) DEFAULT NULL,
  `paym_total_amount` varchar(45) DEFAULT NULL,
  `paym_charge_remark` varchar(45) DEFAULT NULL,
  `paym_due_amount` varchar(45) DEFAULT NULL,
  `paym_last_updated` datetime DEFAULT NULL,
  `paym_last_updated_by` varchar(45) DEFAULT NULL,
  `paym_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`paym_id`),
  UNIQUE KEY `paym_id_UNIQUE` (`paym_id`),
  KEY `fk_paym_user_id_idx` (`paym_user_id`),
  KEY `fk_paym_type_code_id_idx` (`paym_type_code_id`),
  KEY `fk_paym_discount_id_idx` (`paym_discount_id`),
  CONSTRAINT `fk_paym_discount_id` FOREIGN KEY (`paym_discount_id`) REFERENCES `pymt_discount` (`dcnt_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_paym_type_code_id` FOREIGN KEY (`paym_type_code_id`) REFERENCES `gn_code` (`code_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_paym_user_id` FOREIGN KEY (`paym_user_id`) REFERENCES `gn_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pymt_payment`
--

LOCK TABLES `pymt_payment` WRITE;
/*!40000 ALTER TABLE `pymt_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `pymt_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pymt_receipt`
--

DROP TABLE IF EXISTS `pymt_receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pymt_receipt` (
  `rcpt_id` varchar(60) NOT NULL,
  `rcpt_status_code_id` varchar(60) DEFAULT NULL,
  `rcpt_user_id` varchar(60) DEFAULT NULL,
  `rcpt_payment_id` varchar(60) DEFAULT NULL,
  `rcpt_receipt_number` varchar(45) DEFAULT NULL,
  `rcpt_date` datetime DEFAULT NULL,
  `rcpt_printed` varchar(1) DEFAULT NULL,
  `rcpt_payer` varchar(85) DEFAULT NULL,
  `rcpt_notes` varchar(255) DEFAULT NULL,
  `rcpt_last_updated` datetime DEFAULT NULL,
  `rcpt_last_updated_by` varchar(45) DEFAULT NULL,
  `rcpt_timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`rcpt_id`),
  UNIQUE KEY `rcpt_id_UNIQUE` (`rcpt_id`),
  KEY `fk_rcpt_status_code_id_idx` (`rcpt_status_code_id`),
  KEY `fk_rcpt_user_id_idx` (`rcpt_user_id`),
  KEY `fk_rcpt_payment_id_idx` (`rcpt_payment_id`),
  CONSTRAINT `fk_rcpt_payment_id` FOREIGN KEY (`rcpt_payment_id`) REFERENCES `pymt_payment` (`paym_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rcpt_status_code_id` FOREIGN KEY (`rcpt_status_code_id`) REFERENCES `gn_code` (`code_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rcpt_user_id` FOREIGN KEY (`rcpt_user_id`) REFERENCES `gn_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pymt_receipt`
--

LOCK TABLES `pymt_receipt` WRITE;
/*!40000 ALTER TABLE `pymt_receipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `pymt_receipt` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-25 21:28:07
