package imarti.microservices.admin.event;

import static imarti.core.event.config.HibernateEventUtils.setEntityData;

import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreUpdateEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import imarti.core.event.hibernate.annotation.HibernateEventListener;
import imarti.core.persist.GnCity;
import imarti.core.persist.GnCountry;
import imarti.core.persist.GnState;

@Service
public class AddressEvent {

	@HibernateEventListener
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void handlePreInsert(GnCountry entity, PreInsertEvent event) {
		setEntityData("ctry", event);

	}

	@HibernateEventListener
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void handlePreUpdateEvent(GnCountry entity, PreUpdateEvent event) {
		setEntityData("ctry", event);

	}

	@HibernateEventListener
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void handlePreInsert(GnState entity, PreInsertEvent event) {
		setEntityData("stae", event);

	}

	@HibernateEventListener
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void handlePreUpdateEvent(GnState entity, PreUpdateEvent event) {
		setEntityData("stae", event);

	}

	@HibernateEventListener
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void handlePreInsert(GnCity entity, PreInsertEvent event) {
		setEntityData("city", event);

	}

	@HibernateEventListener
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void handlePreUpdateEvent(GnCity entity, PreUpdateEvent event) {
		setEntityData("city", event);

	}
}
