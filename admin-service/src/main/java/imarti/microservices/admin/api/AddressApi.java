package imarti.microservices.admin.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import imarti.core.exception.LayeredException;
import imarti.microservices.admin.dto.CityRequest;
import imarti.microservices.admin.dto.CityResponse;
import imarti.microservices.admin.dto.CountryRequest;
import imarti.microservices.admin.dto.CountryResponse;
import imarti.microservices.admin.dto.StateRequest;
import imarti.microservices.admin.dto.StateResponse;
import imarti.microservices.admin.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = "Address Api")
@RequestMapping(value = "/adm", produces = { MediaType.APPLICATION_JSON_VALUE })
public class AddressApi {

	private static final Logger logger = LogManager.getLogger(AddressApi.class);

	@Autowired
	AddressService addressService;

	@ApiOperation(value = "Add Country", notes = "Country", response = CountryResponse.class)
	@PostMapping(value = "/addCountry")
	public CountryResponse addCountry(@RequestBody CountryRequest countryRequest) throws LayeredException {

		logger.debug("Add Country by countryRequest: {}", countryRequest);
		return addressService.addCountry(countryRequest);
	}

	@ApiOperation(value = "Add State", notes = "State", response = StateResponse.class)
	@PostMapping(value = "/addState")
	public StateResponse addState(@RequestParam(name = "staeCtryId", required = true) String staeCtryId,
			@RequestBody StateRequest stateRequest) throws LayeredException {

		logger.debug("Add State by stateRequest: {}", stateRequest);
		return addressService.addState(staeCtryId, stateRequest);
	}

	@ApiOperation(value = "Add City", notes = "City", response = CityResponse.class)
	@PostMapping(value = "/addCity")
	public CityResponse addCity(@RequestParam(name = "cityStateId", required = true) String cityStateId,
			@RequestBody CityRequest cityRequest) throws LayeredException {

		logger.debug("Add City by cityRequest: {}", cityRequest);
		return addressService.addCity(cityStateId, cityRequest);
	}

	@ApiOperation(value = "Update Country", notes = "Update Country", response = CountryResponse.class)
	@PostMapping(value = "/updateCountry")
	public CountryResponse updateCountry(@RequestParam(name = "countryId", required = true) String ctryId,
			@RequestBody CountryRequest countryRequest) throws LayeredException {

		logger.debug("Update Country by ctryId: {} countryRequest: {}", ctryId, countryRequest);
		return addressService.updateCountry(ctryId, countryRequest);
	}

	@ApiOperation(value = "Update State", notes = "Update State", response = StateResponse.class)
	@PostMapping(value = "/updateState")
	public StateResponse updateState(@RequestParam(name = "stateId", required = true) String staeId,
			@RequestBody StateRequest stateRequest) throws LayeredException {

		logger.debug("Update State by stateId: {} stateRequest: {}", staeId, stateRequest);
		return addressService.updateState(staeId, stateRequest);
	}

	@ApiOperation(value = "Update City", notes = "Update City", response = CityResponse.class)
	@PostMapping(value = "/updateCity")
	public CityResponse updateCity(@RequestParam(name = "cityId", required = true) String cityId,
			@RequestBody CityRequest cityRequest) throws LayeredException {

		logger.debug("Update City by cityId: {} cityRequest: {}", cityId, cityRequest);
		return addressService.updateCity(cityId, cityRequest);
	}

	@ApiOperation(value = "Delete Country", notes = "Delete Country", response = CountryResponse.class)
	@DeleteMapping(value = "/deleteCountry")
	public CountryResponse deleteCountry(@RequestParam(name = "ctryId", required = true) String ctryId,
			@RequestBody CountryRequest countryRequest) throws LayeredException {

		logger.debug("Delete Country by ctryId: {} countryRequest: {}", ctryId, countryRequest);
		return addressService.deleteCountry(ctryId, countryRequest);
	}

	@ApiOperation(value = "Delete State", notes = "Delete State", response = StateResponse.class)
	@DeleteMapping(value = "/deleteState")
	public StateResponse deleteState(@RequestParam(name = "staeId", required = true) String staeId,
			@RequestBody StateRequest stateRequest) throws LayeredException {

		logger.debug("Delete State by stateId: {} stateRequest: {}", staeId, stateRequest);
		return addressService.deleteState(staeId, stateRequest);
	}

	@ApiOperation(value = "Delete City", notes = "Delete City", response = CityResponse.class)
	@DeleteMapping(value = "/deleteCity")
	public CityResponse deleteCity(@RequestParam(name = "cityId", required = true) String cityId,
			@RequestBody CityRequest cityRequest) throws LayeredException {

		logger.debug("Delete City by cityId: {} cityRequest: {}", cityId, cityRequest);
		return addressService.deleteCity(cityId, cityRequest);
	}

	@ApiOperation(value = "Get Country", notes = "Get Country")
	@GetMapping(value = "/getCountry")
	public CountryResponse getCountry(@RequestParam(name = "countryId", required = true) String ctryId)
			throws LayeredException {

		logger.debug("Get Country by ctryId: {}", ctryId);
		return addressService.getCountry(ctryId);
	}

	@ApiOperation(value = "Get State", notes = "Get State")
	@GetMapping(value = "/getState")
	public StateResponse getState(@RequestParam(name = "staeId", required = true) String staeId)
			throws LayeredException {

		logger.debug("Get State by stateId: {}", staeId);
		return addressService.getState(staeId);
	}

	@ApiOperation(value = "Get City", notes = "Get City")
	@GetMapping(value = "/getCity")
	public CityResponse getCity(@RequestParam(name = "cityId", required = true) String cityId) throws LayeredException {

		logger.debug("Get City by cityId: {}", cityId);
		return addressService.getCity(cityId);
	}
}
