package imarti.microservices.admin.config;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

@Configuration
@Import({ AdminSwaggerConfig.class })
@ComponentScan(basePackages = { "imarti.microservices.admin" })
@EnableWebMvc
public class AdminMvcConfig extends AcceptHeaderLocaleResolver implements WebMvcConfigurer {

	private static final List<Locale> LOCALES = Arrays.asList(Locale.US, Locale.FRANCE);

	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("**").addResourceLocations("classpath:/dist/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");

	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
		factory.setValidationMessageSource(messageSource());
		return factory;
	}

	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
		MethodValidationPostProcessor processor = new MethodValidationPostProcessor();
		processor.setValidator(validator());
		return new MethodValidationPostProcessor();
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public Validator getValidator() {
		return validator();
	}

	@Bean
	public MessageSource messageSource() {
		String[] i18nPath = { "classpath:i18n/bean/beanMessages", "classpath:i18n/error/errorMessages",
				"classpath:i18n/swagger/swaggerMessages" };
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames(i18nPath);
		messageSource.setDefaultEncoding(StandardCharsets.UTF_8.displayName());
		messageSource.setCacheSeconds(10);
		return messageSource;
	}

	/**
	 * an interceptor bean that will switch to a new locale based on the value of
	 * the language header parameter appended to a request:
	 * <p>
	 * 
	 * @language should be the name of the request header param i.e
	 *           localhost:8010/api/url with header
	 *           <p>
	 *           Note: All requests to the backend needing Internationalization
	 *           should have the "language" request header param other wise default
	 *           value is english
	 */
	@Override
	public Locale resolveLocale(HttpServletRequest request) {
		String headerLang = request.getHeader("Accept-Language");
		return headerLang == null || headerLang.isEmpty() ? Locale.getDefault()
				: Locale.lookup(Locale.LanguageRange.parse(headerLang), LOCALES);
	}

}
