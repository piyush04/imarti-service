package imarti.microservices.admin.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * Create bean and provide application label object
 * 
 * @author AbhinitKumar
 *
 */
@Configuration
@ComponentScan(basePackages = { "imarti.microservices.admin" })
public class AdminAppConfig {

}
