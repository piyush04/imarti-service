package imarti.microservices.admin.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * Swagger configuration
 * @author AbhinitKumar
 *
 */
@EnableSwagger2
@PropertySource(value = { "classpath:i18n/swagger/swaggerMessages.properties" })
public class AdminSwaggerConfig {

	@Autowired
	private Environment environment;

	@Bean
	public Docket api() {
		// Adding Header

		ParameterBuilder aParameterBuilder = new ParameterBuilder();
		aParameterBuilder.name("Accept-Language").description(environment.getRequiredProperty("lang.hearder.des"))
				.modelRef(new ModelRef("string")).parameterType("header").required(false).build();
		List<Parameter> aParameters = new ArrayList<Parameter>();
		aParameters.add(aParameterBuilder.build());

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("imarti.microservices.admin.api"))
				.paths(PathSelectors.ant("/adm/**")).build().apiInfo(apiInfo()).globalOperationParameters(aParameters);
	}

	/**
	 * Configuration swagger description
	 * 
	 * @return {@code ApiInfo}
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(environment.getRequiredProperty("swagger.title"))
				.description(environment.getRequiredProperty("swagger.description"))
				.version(environment.getRequiredProperty("swagger.version"))
				.contact(new Contact(environment.getRequiredProperty("swagger.contact.name"),
						environment.getRequiredProperty("swagger.contact.url"),
						environment.getRequiredProperty("swagger.contact.email")))
				.license("swagger.license").build();
	}
}
