package imarti.microservices.admin.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import imarti.core.custom.validator.annotation.CapsLetter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CountryRequest {

	@NotEmpty(message = "{ADMB_0006}")
	@CapsLetter(message = "{ADMB_0004}")
	@Size(min = 1, max = 4, message = "{ADMB_0002}")
	@ApiModelProperty(name = "ctryCode", example = "IND", position = 0, notes = "Country Code.", dataType = "String")
	private String ctryCode;

	@NotEmpty(message = "{ADMB_0006}")
	@ApiModelProperty(name = "ctryName", example = "India", position = 1, notes = "Country Name.", dataType = "String")
	private String ctryName;

	@ApiModelProperty(name = "ctryLastUpdatedBy", example = "", position = 2, notes = "Last Updated By.", dataType = "String")
	private String ctryLastUpdatedBy;

}
