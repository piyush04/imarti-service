package imarti.microservices.admin.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import imarti.core.custom.validator.annotation.CapsLetter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CityRequest {

	@NotEmpty(message = "{ADMB_0006}")
	@ApiModelProperty(name = "cityStateId", example = "1000001", position = 0, notes = "City State Id.", dataType = "String")
	private String cityStateId;

	@NotEmpty(message = "{ADMB_0006}")
	@CapsLetter(message = "{ADMB_0004}")
	@Size(min = 1, max = 4, message = "{ADMB_0002}")
	@ApiModelProperty(name = "cityCode", example = "PUN", position = 1, notes = "City Code.", dataType = "String")
	private String cityCode;

	@NotEmpty(message = "{ADMB_0006}")
	@ApiModelProperty(name = "cityName", example = "Pune", position = 2, notes = "City Name.", dataType = "String")
	private String cityName;

	@ApiModelProperty(name = "cityLastUpdatedBy", example = "", position = 3, notes = "Last Updated By.", dataType = "String")
	private String cityLastUpdatedBy;

}
