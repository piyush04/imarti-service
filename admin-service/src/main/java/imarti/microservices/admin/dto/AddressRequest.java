package imarti.microservices.admin.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AddressRequest {

	@ApiModelProperty(name = "Country", position = 0, notes = "Users information", required = true)
	private CountryRequest countryRequest;

	@ApiModelProperty(name = "State", position = 1, notes = "Users information", required = true)
	private StateRequest stateRequest;

	@ApiModelProperty(name = "City", position = 2, notes = "Users information", required = true)
	private CityRequest cityRequest;

	@NotEmpty(message = "{ADMB_0006}")
	@ApiModelProperty(name = "adrsAddressLine1", example = "XYZ", position = 3, notes = "Address Line 1.", dataType = "String")
	private String adrsAddressLine1;

	@ApiModelProperty(name = "adrsAddressLine2", example = "ABD", position = 4, notes = "Address Line 2.", dataType = "String")
	private String adrsAddressLine2;

	@ApiModelProperty(name = "adrsAddressLine3", example = "ABCD", position = 5, notes = "Address Line 3.", dataType = "String")
	private String adrsAddressLine3;

	@NotEmpty(message = "{ADMB_0006}")
	@Size(min = 1, max = 10, message = "{ADMB_0002}")
	@ApiModelProperty(name = "adrsPinCode", example = "411017", position = 6, notes = "Pin Code.", dataType = "String")
	private String adrsPinCode;

	@ApiModelProperty(name = "adrsLastUpdatedBy", example = "", position = 7, notes = "Last Updated By.", dataType = "String")
	private String adrsLastUpdatedBy;

}
