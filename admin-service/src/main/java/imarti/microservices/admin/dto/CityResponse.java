package imarti.microservices.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityResponse extends CityRequest {

	@ApiModelProperty(name = "cityId", example = "01", position = 0, notes = "City Id.", dataType = "String")
	private String cityId;

}
