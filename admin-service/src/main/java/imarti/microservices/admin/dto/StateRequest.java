package imarti.microservices.admin.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import imarti.core.custom.validator.annotation.CapsLetter;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StateRequest {

	@NotEmpty(message = "{ADMB_0006}")
	@ApiModelProperty(name = "staeCountryId", example = "1000001", position = 0, notes = "State Country Id.", dataType = "String")
	private String staeCountryId;

	@NotEmpty(message = "{ADMB_0006}")
	@CapsLetter(message = "{ADMB_0004}")
	@Size(min = 1, max = 4, message = "{ADMB_0002}")
	@ApiModelProperty(name = "staeCode", example = "MAH", position = 1, notes = "State Code.", dataType = "String")
	private String staeCode;

	@NotEmpty(message = "{ADMB_0006}")
	@ApiModelProperty(name = "staeName", example = "Maharashtra", position = 2, notes = "State Name.", dataType = "String")
	private String staeName;

	@NotEmpty(message = "{ADMB_0006}")
	@ApiModelProperty(name = "staeLastUpdatedBy", example = "", position = 3, notes = "Last Updated By.", dataType = "String")
	private String staeLastUpdatedBy;

}
