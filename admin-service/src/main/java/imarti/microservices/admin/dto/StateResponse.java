package imarti.microservices.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StateResponse extends StateRequest {

	@ApiModelProperty(name = "staeId", example = "01", position = 0, notes = "State Id.", dataType = "String")
	private String staeId;

}
