package imarti.microservices.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryResponse extends CountryRequest {

	@ApiModelProperty(name = "ctryId", example = "01", position = 0, notes = "Country Id.", dataType = "String")
	private String ctryId;
}
