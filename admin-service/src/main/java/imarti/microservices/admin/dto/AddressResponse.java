package imarti.microservices.admin.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressResponse extends AddressRequest {

	@ApiModelProperty(name = "adrsId", example = "01", position = 0, notes = "Address Id.", dataType = "String")
	private String adrsId;

}
