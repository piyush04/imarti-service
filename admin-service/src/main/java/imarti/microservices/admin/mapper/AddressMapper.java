package imarti.microservices.admin.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import imarti.core.persist.GnCity;
import imarti.core.persist.GnCountry;
import imarti.core.persist.GnState;
import imarti.microservices.admin.dto.CityRequest;
import imarti.microservices.admin.dto.CityResponse;
import imarti.microservices.admin.dto.CountryRequest;
import imarti.microservices.admin.dto.CountryResponse;
import imarti.microservices.admin.dto.StateRequest;
import imarti.microservices.admin.dto.StateResponse;

@Mapper(componentModel = "spring")
public interface AddressMapper {

	public AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

	public GnCountry gnCountryToCountry(CountryRequest countryRequest);

	public CountryResponse countryTognCountry(GnCountry gnCountry);

	public GnState gnStateToState(StateRequest stateRequest);

	public StateResponse stateTognState(GnState gnState);

	public GnCity gnCityToCity(CityRequest cityRequest);

	public CityResponse cityTopscmCity(GnCity gnCity);

	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	public void updateGnCountryFromCountryRequest(CountryRequest countryRequest, @MappingTarget GnCountry gnCountry);

	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	public void updateGnStateFromStateRequest(StateRequest stateRequest, @MappingTarget GnState gnState);

	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	public void updateGnCityFromCityRequest(CityRequest cityRequest, @MappingTarget GnCity gnCity);

}
