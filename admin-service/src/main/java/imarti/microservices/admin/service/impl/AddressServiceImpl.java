package imarti.microservices.admin.service.impl;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import imarti.core.dao.CityDAO;
import imarti.core.dao.CodeDAO;
import imarti.core.dao.CountryDAO;
import imarti.core.dao.StateDAO;
import imarti.core.exception.BusinessException;
import imarti.core.exception.ServiceException;
import imarti.core.exception.ValidationException;
import imarti.core.persist.GnCountry;
import imarti.core.util.DataUtil;
import imarti.microservices.admin.dto.CityRequest;
import imarti.microservices.admin.dto.CityResponse;
import imarti.microservices.admin.dto.CountryRequest;
import imarti.microservices.admin.dto.CountryResponse;
import imarti.microservices.admin.dto.StateRequest;
import imarti.microservices.admin.dto.StateResponse;
import imarti.microservices.admin.mapper.AddressMapper;
import imarti.microservices.admin.message.AdminErrorCode;
import imarti.microservices.admin.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService {

	private static final Logger logger = LogManager.getLogger(AddressServiceImpl.class);

	@Autowired
	private CountryDAO countryDAO;

	@Autowired
	private StateDAO stateDAO;

	@Autowired
	private CityDAO cityDAO;

	@Autowired
	private CodeDAO codeDAO;

	@Autowired
	AddressMapper addressMapper;

	@Override
	public CountryResponse addCountry(CountryRequest countryRequest) throws ServiceException {

		GnCountry gnCountry = addressMapper.gnCountryToCountry(countryRequest);
		countryDAO.saveCountry(gnCountry);

		logger.debug("Add Country by countryRequest: {}", countryRequest);
		return addressMapper.countryTognCountry(gnCountry);

	}

	@Override
	public CountryResponse updateCountry(String ctryId, CountryRequest countryRequest) throws ServiceException {
		Optional<GnCountry> gnCountry = validateGnCountry(ctryId);

		if (!gnCountry.isPresent()) {
			throw new BusinessException(AdminErrorCode.ADME_0002, DataUtil.CTRYID);
		}
		addressMapper.updateGnCountryFromCountryRequest(countryRequest, gnCountry.get());
		countryDAO.saveCountry(gnCountry.get());

		logger.debug("Update Country by ctryId: {} countryRequest: {}", ctryId, countryRequest);
		return addressMapper.countryTognCountry(gnCountry.get());
	}

	@Override
	public CountryResponse deleteCountry(String ctryId, CountryRequest countryRequest) throws ServiceException {
		Optional<GnCountry> gnCountry = validateGnCountry(ctryId);
		if (gnCountry == null) {
			throw new BusinessException(AdminErrorCode.ADME_0002, DataUtil.CTRYID);
		}
		countryDAO.deleteCountryByCtryId(ctryId);

		logger.debug("Delete Country by ctryId: {} countryRequest: {}", ctryId, countryRequest);
		return addressMapper.countryTognCountry(gnCountry.get());
	}

	@Override
	public CountryResponse getCountry(String ctryId) throws ServiceException {
		Optional<GnCountry> gnCountry = validateGnCountry(ctryId);
		if (gnCountry == null) {
			throw new BusinessException(AdminErrorCode.ADME_0002, DataUtil.CTRYID);
		}
		countryDAO.getCountryByCtryId(ctryId);

		logger.debug("Get Country by ctryId: {}", ctryId);
		return addressMapper.countryTognCountry(gnCountry.get());
	}

	private Optional<GnCountry> validateGnCountry(String ctryId) throws ValidationException {
		Optional<GnCountry> gnCountry = null;

		if (ctryId != null) {
			gnCountry = countryDAO.getCountryByCtryId(ctryId);
			if (!gnCountry.isPresent()) {
				throw new ValidationException(AdminErrorCode.ADME_0004, DataUtil.CTRYID);
			}
		}
		logger.debug("Validate Country by ctryId: {}", ctryId);
		return gnCountry;
	}

	@Override
	public StateResponse addState(String staeCtryId, StateRequest stateRequest) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CityResponse addCity(String cityStateId, CityRequest cityRequest) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StateResponse updateState(String staeId, StateRequest stateRequest) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CityResponse updateCity(String cityId, CityRequest cityRequest) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StateResponse deleteState(String staeId, StateRequest stateRequest) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CityResponse deleteCity(String cityId, CityRequest cityRequest) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StateResponse getState(String staeId) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CityResponse getCity(String cityId) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

}