package imarti.microservices.admin.service;

import org.springframework.stereotype.Service;

import imarti.core.exception.ServiceException;
import imarti.microservices.admin.dto.CityRequest;
import imarti.microservices.admin.dto.CityResponse;
import imarti.microservices.admin.dto.CountryRequest;
import imarti.microservices.admin.dto.CountryResponse;
import imarti.microservices.admin.dto.StateRequest;
import imarti.microservices.admin.dto.StateResponse;

@Service
public interface AddressService {

	public CountryResponse addCountry(CountryRequest countryRequest) throws ServiceException;

	public StateResponse addState(String staeCtryId, StateRequest stateRequest) throws ServiceException;

	public CityResponse addCity(String cityStateId, CityRequest cityRequest) throws ServiceException;

	public CountryResponse updateCountry(String ctryId, CountryRequest countryRequest) throws ServiceException;

	public StateResponse updateState(String staeId, StateRequest stateRequest) throws ServiceException;

	public CityResponse updateCity(String cityId, CityRequest cityRequest) throws ServiceException;

	public CountryResponse deleteCountry(String ctryId, CountryRequest countryRequest) throws ServiceException;

	public StateResponse deleteState(String staeId, StateRequest stateRequest) throws ServiceException;

	public CityResponse deleteCity(String cityId, CityRequest cityRequest) throws ServiceException;

	public CountryResponse getCountry(String ctryId) throws ServiceException;

	public StateResponse getState(String staeId) throws ServiceException;

	public CityResponse getCity(String cityId) throws ServiceException;

}
