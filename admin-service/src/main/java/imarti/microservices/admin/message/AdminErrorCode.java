package imarti.microservices.admin.message;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import imarti.core.message.FaultCode;

/**
 * @author AbhinitKumar
 *
 */
public enum AdminErrorCode implements FaultCode {

	ADME_0001, ADME_0002, ADME_0003, ADME_0004;

	private static final String PATH = "classpath:i18n/error/errorMessages";
	private static ReloadableResourceBundleMessageSource resource;
	private static final Logger logger = LogManager.getLogger(AdminErrorCode.class);

	@Autowired
	MessageSource bundleMessageSource;

	public String getKey() {
		return toString();
	}

	@Override
	public ReloadableResourceBundleMessageSource getBundle() {
		try {
			logger.info("Start loading property file.");
			resource = new ReloadableResourceBundleMessageSource();
			resource.setBasename(PATH);
			resource.setDefaultEncoding("UTF-8");
			logger.info("Property file loaded.");

		} catch (Exception e) {
			logger.debug("Error in loading roperty file", e);
			logger.error("Start loading property file.", e);
		}
		return resource;
	}

}
