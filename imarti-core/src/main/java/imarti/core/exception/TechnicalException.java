package imarti.core.exception;

import imarti.core.message.FaultCode;

/**
 * <p>
 * Sub-classes {@link imarti.core.LayeredException}.
 * </p>
 * <p>
 * Root level hierarchy for errors that are related to a technical level.<br>
 * </p>
 * <p>
 * This exception should not be thrown for errors such as invalid user input or
 * data access errors. Basically, this represents a defect in the program or its
 * runtime configuration. This is a very general top-level exception. Before
 * creating and throwing a new instance of this type, consider if a more
 * specific subclass would be more appropriate.
 * </p>
 */
public class TechnicalException extends LayeredException {

	private static final long serialVersionUID = 1L;

	public TechnicalException(Throwable cause, FaultCode faultCode, Object... messageArguments) {
		super(cause, faultCode, messageArguments);
	}

	public TechnicalException(FaultCode faultCode, Object... messageArguments) {
		super(faultCode, messageArguments);
	}

	public TechnicalException(LayeredException le) {
		super(le);
	}

}
