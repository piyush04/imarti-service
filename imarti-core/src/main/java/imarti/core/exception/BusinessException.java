package imarti.core.exception;

import imarti.core.message.FaultCode;

/**
 * Root level hierarchy for Exceptions that are related to a business level
 * logic.<br>
 * Refers to any high level Exception.
 */
public class BusinessException extends ServiceException {

	private static final long serialVersionUID = 1L;

	public BusinessException(Throwable cause, FaultCode faultCode, Object... messageArguments) {
		super(cause, faultCode, messageArguments);
	}

	public BusinessException(FaultCode faultCode, Object... messageArguments) {
		super(faultCode, messageArguments);
	}

	public BusinessException(BusinessException be) {
		super(be);
	}

}
