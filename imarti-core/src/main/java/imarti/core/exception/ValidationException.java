package imarti.core.exception;

import imarti.core.message.FaultCode;

public class ValidationException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public ValidationException(FaultCode faultCode, Object... messageArguments) {
		super(faultCode, messageArguments);
	}

	public ValidationException(Throwable cause, FaultCode faultCode, Object... messageArguments) {
		super(cause, faultCode, messageArguments);
	}

	public ValidationException(BusinessException se) {
		super(se);
	}
}
