package imarti.core.exception;

import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import imarti.core.ErrorDetails;

/**
 * 
 * Global Exception handled in all micro-services.
 * 
 * @author AbhinitKumar
 *
 */
@RestControllerAdvice
@RestController
public final class ResponseException extends ResponseEntityExceptionHandler {

	private static final Logger log = LogManager.getLogger(ResponseException.class);

	private ResponseException() {
	}

	@Autowired
	private MessageSource messageSource;

	// @formatter:off
	@ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
	private final ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		log.debug(getErrorMessage(ExceptionType.RUNTIME_EXCEPTION, ExceptionType.SERVER_ERROR, ex.getMessage()), ex);
		log.error(getErrorMessage(ExceptionType.RUNTIME_EXCEPTION, ExceptionType.SERVER_ERROR, ex.getMessage()), ex);
		return new ResponseEntity<Object>(
				new ErrorDetails(ExceptionType.RUNTIME_EXCEPTION, ExceptionType.SERVER_ERROR, ex.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);

	}

	// @formatter:off
	@ExceptionHandler(Exception.class)
	private final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.debug(getErrorMessage(ExceptionType.GLOBAL_EXCEPTION, ExceptionType.SERVER_ERROR, ex.getMessage()), ex);
		log.error(getErrorMessage(ExceptionType.GLOBAL_EXCEPTION, ExceptionType.SERVER_ERROR, ex.getMessage()), ex);
		return new ResponseEntity<Object>(
				new ErrorDetails(ExceptionType.GLOBAL_EXCEPTION, ExceptionType.SERVER_ERROR, ex.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	// @formatter:off
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		log.debug(getErrorMessage(ExceptionType.JSON_FORMAT, ExceptionType.JSON_ERROR, ex.getMessage()), ex);
		log.error(getErrorMessage(ExceptionType.JSON_FORMAT, ExceptionType.JSON_ERROR, ex.getMessage()), ex);
		return new ResponseEntity<Object>(
				new ErrorDetails(ExceptionType.JSON_FORMAT, ExceptionType.JSON_ERROR, ex.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);

	}

	// @formatter:off
	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<ErrorDetails> constraintViolationException(ConstraintViolationException ex,
			HttpServletResponse response) {
		String message = null;
		String code = null;
		Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
		for (ConstraintViolation<?> constraintViolation : constraintViolations) {
			message = getErrorMessage(constraintViolation);
			code = constraintViolation.getMessage().replace("{", "").replace("}", "").trim();
			break;
		}

		log.debug(getErrorMessage(ExceptionType.BEAN_EXCEPTION, code, message), ex);
		log.error(getErrorMessage(ExceptionType.BEAN_EXCEPTION, code, message), ex);
		return new ResponseEntity<ErrorDetails>(new ErrorDetails(ExceptionType.BEAN_EXCEPTION, code, message),
				HttpStatus.BAD_REQUEST);

	}

	private final String getErrorMessage(ConstraintViolation<?> constraintViolation) {
		String attribute = constraintViolation.getPropertyPath().toString();
		Object value = constraintViolation.getInvalidValue();
		String[] attributes = attribute.split("\\.");

		if (attributes.length < 2)
			attribute = attributes[0];
		else
			attribute = attributes[(attributes.length) - 1];
		attribute = attribute + ": " + String.valueOf(value);
		String code = constraintViolation.getMessage().replace("{", "").replace("}", "").trim();
		return messageSource.getMessage(code, null, LocaleContextHolder.getLocale()).replace("${validatedValue}",
				attribute);
	}

	// @formatter:off
	@Override
	protected final ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		/*
		 * List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x
		 * -> getError(x)) .collect(Collectors.toList());
		 * ex.getBindingResult().getModel();
		 */
		FieldError objectError = ex.getBindingResult().getFieldErrors().get(0);
		log.debug(
				getErrorMessage(ExceptionType.BEAN_EXCEPTION, objectError.getField(), objectError.getDefaultMessage()),
				ex);
		log.error(
				getErrorMessage(ExceptionType.BEAN_EXCEPTION, objectError.getField(), objectError.getDefaultMessage()),
				ex);
		return new ResponseEntity<Object>(
				new ErrorDetails(ExceptionType.BEAN_EXCEPTION, objectError.getField(), objectError.getDefaultMessage()),
				status);

	}

	// @formatter:off
	@ExceptionHandler(LayeredException.class)
	private final ResponseEntity<ErrorDetails> layeredExceptionHandler(LayeredException le) {
		log.debug(getErrorMessage(ExceptionType.LAYER_EXCEPTION, le.getErrorCode(), le.getErrorMessage()), le);
		log.error(getErrorMessage(ExceptionType.LAYER_EXCEPTION, le.getErrorCode(), le.getErrorMessage()), le);
		return new ResponseEntity<ErrorDetails>(
				new ErrorDetails(ExceptionType.LAYER_EXCEPTION, le.getErrorCode(), le.getErrorMessage()),
				HttpStatus.UNPROCESSABLE_ENTITY);

	}

	// @formatter:off
	@ExceptionHandler(ServiceException.class)
	private final ResponseEntity<ErrorDetails> serviceExceptionHandler(ServiceException se) {
		log.debug(getErrorMessage(ExceptionType.SERVICE_EXCEPTION, se.getErrorCode(), se.getErrorMessage()), se);
		log.error(getErrorMessage(ExceptionType.SERVICE_EXCEPTION, se.getErrorCode(), se.getErrorMessage()), se);
		return new ResponseEntity<ErrorDetails>(
				new ErrorDetails(ExceptionType.SERVICE_EXCEPTION, se.getErrorCode(), se.getErrorMessage()),
				HttpStatus.UNPROCESSABLE_ENTITY);

	}

	private final String getErrorMessage(String exceptionType, String errorCode, String message) {
		String errorMessage = "";
		if (errorCode != null && exceptionType != null)
			errorMessage = "(" + exceptionType + ") " + errorCode + ": ";
		if (message != null)
			errorMessage = errorMessage + message;
		return errorMessage;
	}

	// @formatter:off
	@ExceptionHandler(BusinessException.class)
	private final ResponseEntity<ErrorDetails> businessExceptionHandler(BusinessException be) {
		log.debug(getErrorMessage(ExceptionType.BUSINESS_EXCEPTION, be.getErrorCode(), be.getErrorMessage()), be);
		log.error(getErrorMessage(ExceptionType.BUSINESS_EXCEPTION, be.getErrorCode(), be.getErrorMessage()), be);
		return new ResponseEntity<ErrorDetails>(
				new ErrorDetails(ExceptionType.BUSINESS_EXCEPTION, be.getErrorCode(), be.getErrorMessage()),
				HttpStatus.UNPROCESSABLE_ENTITY);

	}

	// @formatter:off
	@ExceptionHandler(TechnicalException.class)
	private final ResponseEntity<ErrorDetails> technicalExceptionHandler(TechnicalException te) {
		log.debug(getErrorMessage(ExceptionType.TECHNICAL_EXCEPTION, te.getErrorCode(), te.getErrorMessage()), te);
		log.error(getErrorMessage(ExceptionType.TECHNICAL_EXCEPTION, te.getErrorCode(), te.getErrorMessage()), te);
		return new ResponseEntity<ErrorDetails>(
				new ErrorDetails(ExceptionType.TECHNICAL_EXCEPTION, te.getErrorCode(), te.getErrorMessage()),
				HttpStatus.UNPROCESSABLE_ENTITY);

	}

	// @formatter:off
	@ExceptionHandler(ValidationException.class)
	private final ResponseEntity<ErrorDetails> validationExceptionHandler(ValidationException te) {
		log.debug(getErrorMessage(ExceptionType.VALIDATION_EXCEPTION, te.getErrorCode(), te.getErrorMessage()), te);
		log.error(getErrorMessage(ExceptionType.VALIDATION_EXCEPTION, te.getErrorCode(), te.getErrorMessage()), te);
		return new ResponseEntity<ErrorDetails>(
				new ErrorDetails(ExceptionType.VALIDATION_EXCEPTION, te.getErrorCode(), te.getErrorMessage()),
				HttpStatus.UNPROCESSABLE_ENTITY);

	}

}
