package imarti.core.exception;

import imarti.core.message.FaultCode;
import imarti.core.message.FaultInfo;
import imarti.core.message.ServiceCodeHelper;

/**
 * An exception which consolidates nested (and other layered) descriptions from:
 * <ul>
 * <li>Nested cause exceptions</li>
 * <li>SQLExcepton chains of warnings and chains of errors</li>
 * <li>Database error layers, which are CR/LF delimited, due to no exception
 * handling mechanism in the database</li>
 * <li>Other exceptions of this type that are passed into this constructor</li>
 * <li>Arbitrary extra information added by methods on this exception</li>
 * </ul>
 * <p/>
 * The collective layers are presented as a concise chronological list.
 * <p/>
 * The layers a held internally as strings not a linked list of cause exceptions
 * so that the exception can be simply serialized and will be useful over a
 * remote method invocation using RMI (without needing a unknown list of
 * exception classes to be in client side class loader).
 * <p/>
 * 
 */
public abstract class LayeredException extends Exception {

	public static final long serialVersionUID = 1;
	private FaultCode faultCode;

	private Object[] messageArgs;

	/** Exception description for web service stack. */
	private final FaultInfo faultInfo = new FaultInfo();

	/**
	 * Constructor that adds layers to this exception.
	 * 
	 * @param cause            root cause
	 * @param faultCode        error code (referring to message key in related
	 *                         message bundle)
	 * @param messageArguments variables used to populate the message pattern (not
	 *                         mandatory if the message doesn't contain any
	 *                         variable)
	 */
	protected LayeredException(Throwable cause, FaultCode faultCode, Object... messageArguments) {
		super(cause);
		this.faultCode = faultCode;
		this.faultInfo.setFaultCode(this.faultCode.getKey().replace('_', '-'));
		this.faultInfo.setErrorMessage(ServiceCodeHelper.getMessage(faultCode, messageArguments));
		this.faultInfo.setMessage(getMessage());
		this.faultInfo.setErrorClass(this.getClass().getName());
	}

	/**
	 * Constructor adds the first layer with the given fault code, arguments and
	 * severity.
	 * 
	 * @param faultCode        error code (referring to message key in related
	 *                         message bundle)
	 * @param messageArguments variables used to populate the message pattern (not
	 *                         mandatory if the message doesn't contain any
	 *                         variable)
	 */
	protected LayeredException(FaultCode faultCode, Object... messageArguments) {
		this(null, faultCode, messageArguments);
	}

	protected LayeredException(LayeredException e) {
		this(e, null, e.getFaultCode(), e.getMessageArgs());
	}

	@Override
	public String toString() {
		return getMessage();
	}

	/**
	 * Subclasses can override this method to provide a different class name in the
	 * Layer (usually a parent class).
	 * 
	 * @return the className to appear in the Layer.
	 */
	protected String getExceptionClassForLayer() {
		return this.getClass().getName();
	}

	public String getErrorMessage() {
		return this.faultInfo.getErrorMessage();
	}

	public FaultCode getFaultCode() {
		return this.faultCode;
	}

	public Object[] getMessageArgs() {
		return messageArgs;
	}

	public String getErrorCode() {
		return this.faultInfo.getFaultCode();
	}

}
