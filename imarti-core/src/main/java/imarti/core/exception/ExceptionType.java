package imarti.core.exception;

/**
 * Error type contact.
 * 
 * @author AbhinitKumar
 *
 */
public final class ExceptionType {

	private ExceptionType() {
	}

	/**
	 * 
	 */
	public static final String SERVICE_EXCEPTION = "service-exception";
	public static final String LAYER_EXCEPTION = "layer-exception";
	public static final String BUSINESS_EXCEPTION = "business-exception";
	public static final String TECHNICAL_EXCEPTION = "technical-exception";
	public static final String VALIDATION_EXCEPTION = "validation-exception";
	public static final String BEAN_EXCEPTION = "bean-exception";
	public static final String GLOBAL_EXCEPTION = "global-exception";
	public static final String JSON_FORMAT = "json-format";
	public static final String RUNTIME_EXCEPTION = "runtime-exception";
	public static final String SERVER_ERROR = "SERVER-ERROR";
	public static final String JSON_ERROR = "JSON-ERROR";

}
