package imarti.core.exception;

import imarti.core.message.FaultCode;

/**
 * Sub-classes {@link imarti.core.LayeredException}.
 * 
 * <p>
 * General exception class for any errors that are generated in the DAO tier.
 * This class includes user-generated errors that were propagated to the client
 * via the Hibernate and JPA RAISEERROR function.
 * 
 * </p>
 */
public class DAOException extends TechnicalException {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor adds the first layer with the given fault code, arguments and
	 * severity.
	 * 
	 * @param cause            root cause
	 * @param faultCode        error code (referring to message key in related
	 *                         message bundle)
	 * @param messageArguments variables used to populate the message pattern (not
	 *                         mandatory if the message doesn't contain any
	 *                         variable)
	 */
	public DAOException(Throwable cause, FaultCode faultCode, Object... messageArguments) {
		super(cause, faultCode, messageArguments);
	}

	/**
	 * Constructor adds the first layer with the given fault code, arguments and
	 * severity
	 * 
	 * @param faultCode        error code (referring to message key in related
	 *                         message bundle)
	 * @param messageArguments variables used to populate the message pattern (not
	 *                         mandatory if the message doesn't contain any
	 *                         variable)
	 */
	public DAOException(FaultCode faultCode, Object... messageArguments) {
		this(null, faultCode, messageArguments);
	}

	public DAOException(LayeredException e) {
		super(e);
	}

	public DAOException(TechnicalException e) {
		super(e);
	}

}
