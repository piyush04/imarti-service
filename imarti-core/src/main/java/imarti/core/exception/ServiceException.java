package imarti.core.exception;

import imarti.core.message.FaultCode;

/**
 * Indicates an exception occurred running a service.
 */
public class ServiceException extends TechnicalException {

	private static final long serialVersionUID = 1L;

	public ServiceException(FaultCode faultCode, Object... messageArguments) {
		super(faultCode, messageArguments);
	}

	public ServiceException(Throwable cause, FaultCode faultCode, Object... messageArguments) {
		super(cause, faultCode, messageArguments);
	}

	public ServiceException(BusinessException be) {
		super(be);
	}

}
