package imarti.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import imarti.core.exception.BusinessException;
import imarti.core.exception.ValidationException;

/**
 * This class is validate time rule and perform date and time operation.
 * 
 * @author AbhinitKumar
 *
 */
public final class TimeUtils {// TODO make less complicated, probably via StreamTokenizer

	/**
	 * Time pattern validity pattern regular expression for example: "5h55m",
	 * "2.5h", "999999d"
	 */
	private static final Pattern TIME_VALIDITY_PATTERN = Pattern
			.compile("^((?:\\d*\\.?\\d+)(?i)(?:" + Joiner.on('|').join(CalendarUnit.ALL_ALIASES) + "))*$"); // This
																											// pattern
																											// checks
																											// for
																											// validity
																											// in input,
																											// as it
																											// matches
																											// the whole
																											// string.
	private static final Pattern TIME_PATTERN = Pattern
			.compile("((\\d*\\.?\\d+)(?i)(" + Joiner.on('|').join(CalendarUnit.ALL_ALIASES) + "))"); // #(.#) + a time
																										// quantifier

	private static final long MILLIS_PER_SECOND = 86400000;
	private static final String DD_MM_YYYY = "dd-MM-yyyy";
	private static final String HH_MM_SS_A = "hh:mm:ss a";
	private static final String DD_MM_YYYY_HH_MM_SS_A = "dd-MM-yyyy hh:mm:ss a";
	private static final String HH_MM_A = "hh:mm a";

	private static final Comparator<String> STRING_LENGTH_COMPARATOR = new Comparator<String>() {

		@Override
		public int compare(String s1, String s2) {
			return s2.length() - s1.length();
		}
	};

	private TimeUtils() {

	}

	/**
	 * @param ruleTime arguments for example: "5h55m", "2.5h", "999999d"
	 *                 ,"2.5H","2.5hrs","2.5hour"
	 * @return true or false
	 * @throws BusinessException
	 */
	public static Boolean validateRuleTime(String ruleTime) throws ValidationException {

		Boolean flag = false;
		if (ruleTime == null || ruleTime.isEmpty()) {
			// throw new ValidationException(null);

		}
		if (TIME_VALIDITY_PATTERN.matcher(ruleTime).matches()) {
			flag = true;
		} else {
			// throw new ValidationException(null);
		}

		return flag;

	}

	/**
	 * @param time arguments for example: "5h55m", "2.5h", "999999d"
	 *             ,"2.5H","2.5hrs","2.5hour"
	 * @return
	 * @throws BusinessException
	 */
	public static Date addInSystemTime(String time) throws BusinessException {

		Date date = null;

		if (time == null || time.isEmpty()) {
			throw new BusinessException(null);

		}

		if (TIME_VALIDITY_PATTERN.matcher(time).matches()) {
			long timeInMilliSecond = parseMilliseconds(time);
			date = new Date(System.currentTimeMillis() + timeInMilliSecond);
		} else {
			throw new BusinessException(null);
		}

		return date;
	}

	/**
	 * @param time arguments for example: "5h55m", "2.5h", "999999d"
	 *             ,"2.5H","2.5hrs","2.5hour"
	 * @return
	 * @throws BusinessException
	 */
	public static Date subtractInSystemTime(String time) throws BusinessException {

		Date date = null;

		if (time == null || time.isEmpty()) {
			// throw new BusinessException(TransFaultCode.TRANS_0067);

		}

		if (TIME_VALIDITY_PATTERN.matcher(time).matches()) {
			long timeInMilliSecond = parseMilliseconds(time);
			date = new Date(System.currentTimeMillis() - timeInMilliSecond);
		} else {
			// throw new BusinessException(TransFaultCode.TRANS_0003);
		}

		return date;
	}

	public static void main(String[] args) throws BusinessException, ParseException {
		String input = "2.7l";

		if (TIME_VALIDITY_PATTERN.matcher(input).matches()) {
			long time = parseMilliseconds(input);

			Date expireDate = new Date(System.currentTimeMillis() + time);

			Date expire = new Date(System.currentTimeMillis() - time);

			System.out.println("Time here " + expireDate);
			System.out.println("Time here " + expire);
			System.out.println(parseReadableDate(time));
		}
	}

	/**
	 * @param input the input string to be parsed, for example: "5h55m", "2.5h",
	 *              "999999d"
	 * @return the millisecond equivalent of all times added.
	 * 
	 * @throws ValidationException if the string isn't a valid time string, or is
	 *                             bigger than {@link Long#MAX_VALUE}
	 */

	public static long parseMilliseconds(String input) throws ValidationException {

		if (!TIME_VALIDITY_PATTERN.matcher(input).matches()) {
			// throw new ValidationException(TransFaultCode.TRANS_0003);
		}

		Matcher timeSubmatcher = TIME_PATTERN.matcher(input);
		BigInteger ret = BigInteger.ZERO;

		while (timeSubmatcher.find()) {

			String numberPortion = timeSubmatcher.group(2);
			String aliasPortion = timeSubmatcher.group(3).toLowerCase();

			if (CalendarUnit.ALIAS_MAP.containsKey(aliasPortion)) {

				CalendarUnit unit = CalendarUnit.ALIAS_MAP.get(aliasPortion);
				BigDecimal value = new BigDecimal(numberPortion);
				BigDecimal msValue = value.multiply(BigDecimal.valueOf(unit.timeInMillis));
				ret = ret.add(msValue.toBigInteger());

			} else {
				// throw new ValidationException(TransFaultCode.TRANS_0003);
			}
		}

		long longValue = ret.longValue();

		if (ret.compareTo(BigInteger.valueOf(longValue)) == 0) {

			return ret.longValue();

		} else {
			// throw new ValidationException(TransFaultCode.TRANS_0003);
		}
		return longValue;
	}

	/**
	 * 
	 * Change millisecond to readable time and date.
	 * 
	 * @param milliseconds
	 * @return
	 */
	public static String parseReadableDate(long milliseconds) {

		StringBuilder sb = new StringBuilder();

		for (int i = CalendarUnit.values().length - 1; i >= 0; i--) {

			CalendarUnit u = CalendarUnit.values()[i];
			int numUnits = (int) (milliseconds / u.timeInMillis);

			if (numUnits >= 1) {

				sb.append(", ");
				sb.append(numUnits).append(" ");

				if (numUnits >= 2) {
					sb.append(u.mainAliasPlural); // 2 minutes
				} else {
					sb.append(u.mainAlias); // 1 minute
				}
				milliseconds -= numUnits * u.timeInMillis;
			}
		}
		return sb.substring(2);
	}

	public static ImmutableList<String> getTimeUnits() {
		return CalendarUnit.MAIN_ALIASES;
	}

	/**
	 * Using for validate time with {@link Character}
	 *
	 */
	private enum CalendarUnit {

		SECOND(TimeUnit.SECONDS.toMillis(1), "second", "seconds", "s", "sec", "secs"),
		MINUTE(TimeUnit.MINUTES.toMillis(1), "minute", "minutes", "m", "min", "mins"),
		HOUR(TimeUnit.HOURS.toMillis(1), "hour", "hrs", "h", "hr", "hours"),
		DAY(TimeUnit.DAYS.toMillis(1), "day", "days", "d"),
		WEEK(TimeUnit.DAYS.toMillis(1) * 7L, "week", "weeks", "w", "wk", "wks"),
		MONTH(TimeUnit.DAYS.toMillis(1) * 30L, "month", "months", "mn", "mns"),
		YEAR(TimeUnit.DAYS.toMillis(1) * 365L, "year", "years", "y", "yr", "yrs");

		private static ImmutableList<String> ALL_ALIASES = ImmutableList.copyOf(getAllAliases());
		private static ImmutableMap<String, CalendarUnit> ALIAS_MAP = getAliasMap();
		private static ImmutableList<String> MAIN_ALIASES = ImmutableList.copyOf(getMainAliases());

		private static Collection<String> getAllAliases() {

			ArrayList<String> aliases = new ArrayList<>();

			for (CalendarUnit c : values()) {
				Collections.addAll(aliases, c.aliases);
			}

			Collections.sort(aliases, STRING_LENGTH_COMPARATOR); // Sort them by largest to smallest so that the regex
																	// doesn't match a smaller value first.
			return aliases;
		}

		private static ImmutableMap<String, CalendarUnit> getAliasMap() {

			ImmutableMap.Builder<String, CalendarUnit> b = ImmutableMap.builder();

			for (CalendarUnit c : values()) {
				for (String a : c.aliases) {
					b.put(a, c);
				}
			}
			return b.build();
		}

		private static Collection<String> getMainAliases() {

			ArrayList<String> aliases = new ArrayList<>();
			for (CalendarUnit c : values()) {
				aliases.add(c.mainAlias);
			}

			Collections.sort(aliases, STRING_LENGTH_COMPARATOR); // Sort them by largest to smallest so that the regex
																	// doesn't match a smaller value first.
			return aliases;
		}

		private final long timeInMillis;
		private final String[] aliases;
		private final String mainAlias;
		private final String mainAliasPlural;

		CalendarUnit(long timeInMillis, String mainAlias, String mainAliasPlural, String... aliases) {

			this.timeInMillis = timeInMillis;
			this.mainAlias = mainAlias;
			this.mainAliasPlural = mainAliasPlural;
			String[] newAliases = new String[aliases.length + 2];
			newAliases[0] = mainAlias;
			newAliases[1] = mainAliasPlural;
			System.arraycopy(aliases, 0, newAliases, 2, aliases.length);
			this.aliases = newAliases;
		}
	}

	public static Boolean isTimeBefore(String requestTime, String beforeTime) throws BusinessException {

		boolean flag = false;

		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(HH_MM_A);
			Date requestDate = simpleDateFormat.parse(requestTime);
			long requestMillisecond = requestDate.getTime();

			long beforeMilliSecond = TimeUtils.parseMilliseconds(beforeTime);

			String beforeDate = simpleDateFormat.format(new Date(requestMillisecond - beforeMilliSecond));

			Date currentTime = new Date(System.currentTimeMillis());
			String currentStrDate = simpleDateFormat.format(currentTime);

			Date currentDate = simpleDateFormat.parse(currentStrDate);
			Date beforedate = simpleDateFormat.parse(beforeDate);

			if (currentDate.before(beforedate)) {

				flag = true;

			}
		} catch (ParseException e) {
			// throw new SeverException(e, TransFaultCode.REST_SERVER_ERROR);
		}

		return flag;
	}

	public static Boolean isBeforeAndSame(String firstTime, String secondTime) throws BusinessException {

		boolean flag = false;
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(HH_MM_A);
			Date firstDate = simpleDateFormat.parse(firstTime);
			Date secondDate = simpleDateFormat.parse(secondTime);
			if (firstDate.compareTo(secondDate) > 0) {
				flag = true;
			} else if (firstDate.compareTo(secondDate) == 0) {
				flag = false;
			}
		} catch (ParseException e) {
			// throw new SeverException(e, TransFaultCode.REST_SERVER_ERROR);
		}
		return flag;

	}

	public static Boolean isTimeAfter(String requestTime, String afterTime) throws ValidationException {

		boolean flag = false;

		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(HH_MM_A);
			Date requestDate = simpleDateFormat.parse(requestTime);
			long requestMillisecond = requestDate.getTime();

			long beforeMilliSecond = TimeUtils.parseMilliseconds(afterTime);

			String beforeDate = simpleDateFormat.format(new Date(requestMillisecond + beforeMilliSecond));

			Date currentTime = new Date(System.currentTimeMillis());
			String currentStrDate = simpleDateFormat.format(currentTime);

			Date currentDate = simpleDateFormat.parse(currentStrDate);
			Date beforedate = simpleDateFormat.parse(beforeDate);

			if (currentDate.before(beforedate)) {

				flag = true;

			}
		} catch (ParseException e) {
			// throw new ValidationException(TransFaultCode.TRANS_0002);
		}

		return flag;
	}

	public static String unique() {

		Random random = new Random();
		StringBuilder sb = new StringBuilder();
		sb.append(random.nextInt(8) + 1);

		for (int i = 0; i < 9; i++) {
			sb.append(random.nextInt(8));
		}

		return sb.toString();
	}

	public static String unique(String uuid) {
		StringBuilder sb = null;
		if (uuid != null) {
			Random random = new Random();
			sb = new StringBuilder();
			sb.append(uuid + "_");
			sb.append(random.nextInt(3) + 1);

			for (int i = 0; i < 4; i++) {
				sb.append(random.nextInt(3));
			}
		}
		return sb.toString();
	}

	public static Date dateBrfore(Date d, int days) {
		Date date = null;
		if (d != null) {
			date = new Date(d.getTime() - days * MILLIS_PER_SECOND);
		}
		return date;
	}

	public static Date getDate(Date date) throws ValidationException {
		try {
			SimpleDateFormat simpleDateFormat = null;
			String dateString = null;
			if (date != null) {
				simpleDateFormat = new SimpleDateFormat(DD_MM_YYYY);
				dateString = simpleDateFormat.format(date);
				return simpleDateFormat.parse(dateString);
			} else {
				return null;
			}
		} catch (ParseException e) {
			// throw new ValidationException(TransFaultCode.TRANS_0002);
		}
		return date;

	}

	public static Date getDate(String date) throws ValidationException {
		SimpleDateFormat simpleDateFormat = null;

		try {
			if (date != null && !date.isEmpty()) {
				simpleDateFormat = new SimpleDateFormat(DD_MM_YYYY);
				return simpleDateFormat.parse(date);

			} else {
				return null;
			}
		} catch (ParseException e) {
			// throw new ValidationException(TransFaultCode.TRANS_0002);
		}
		return null;

	}

	public static String getFormateDate(Date date) {
		SimpleDateFormat simpleDateFormat = null;
		String dateString = null;
		if (date != null) {
			simpleDateFormat = new SimpleDateFormat(DD_MM_YYYY);
			dateString = simpleDateFormat.format(date);
			return dateString;
		} else {
			return null;
		}
	}

	public static String getFormateTime(Date date) {
		SimpleDateFormat simpleDateFormat = null;
		String dateString = null;
		if (date != null) {
			simpleDateFormat = new SimpleDateFormat(HH_MM_SS_A);
			dateString = simpleDateFormat.format(date);
			return dateString;
		} else {
			return null;
		}
	}

	public static Date getTime(Date date) throws ValidationException {
		try {
			SimpleDateFormat simpleDateFormat = null;
			String dateString = null;
			if (date != null) {
				simpleDateFormat = new SimpleDateFormat(DD_MM_YYYY_HH_MM_SS_A);
				dateString = simpleDateFormat.format(date);
				return simpleDateFormat.parse(dateString);
			} else {
				return null;
			}
		} catch (ParseException e) {
			// throw new ValidationException(TransFaultCode.TRANS_0002);
		}
		return date;

	}

	/**
	 * @param time
	 * @return
	 * @throws ParseException
	 */
	public static boolean isAfterTime(String time) throws ValidationException {
		boolean flag = false;

		try {
			Date beforeTime = new SimpleDateFormat(HH_MM_SS_A).parse(time);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(HH_MM_SS_A);
			Date afterTime = simpleDateFormat.parse(simpleDateFormat.format(new Date()));

			if (beforeTime.before(afterTime)) {
				flag = true;
			}
		} catch (ParseException e) {
			// throw new ValidationException(TransFaultCode.TRANS_0002);
		}

		return flag;
	}

}
