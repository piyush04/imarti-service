package imarti.core.util;

public final class DataUtil {

	private DataUtil() {
	}

	public static final String CTRYID = "ctryId";

	public static final String STAEID = "staeId";

	public static final String CITYID = "cityId";

	public static final String STAECOUNTRYID = "staeCountryId";

	public static final String CITYSTATEID = "cityStateId";

}
