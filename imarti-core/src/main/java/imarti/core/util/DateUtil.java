package imarti.core.util;


/**
 * 
 * Use for Date related operation.
 * <h1>Note: All method must be as static.</h1>
 * <br>
 * 
 * @author AbhinitKumar
 *
 */
public final class DateUtil {

	private DateUtil() {
	}

}
