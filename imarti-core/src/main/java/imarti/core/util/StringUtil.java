package imarti.core.util;

/**
 * 
 * Use for String operation eg : {@link #isCaptalLetter(String) }
 * 
 * @author AbhinitKumar
 *
 */
public final class StringUtil {

	private StringUtil() {
	}

	public static boolean isSmallLetter(String value) {
		char[] charArray = value.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			if (Character.isUpperCase(charArray[i]))
				return false;
		}
		return true;

	}

	public static boolean isCaptalLetter(String value) {
		char[] charArray = value.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			if (!Character.isUpperCase(charArray[i]))
				return false;
		}
		return true;

	}

}
