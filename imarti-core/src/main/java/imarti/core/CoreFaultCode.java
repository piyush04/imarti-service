package imarti.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import imarti.core.message.FaultCode;

/**
 * @author AbhinitKumar
 *
 */
public enum CoreFaultCode implements FaultCode {

	REST_SERVER_ERROR;

	private static final String PATH = "classpath:i18n/messages";
	private static ReloadableResourceBundleMessageSource resource;
	private static final Logger logger = LogManager.getLogger(CoreFaultCode.class);

	@Autowired
	MessageSource bundleMessageSource;

	public String getKey() {
		return toString();
	}

	@Override
	public ReloadableResourceBundleMessageSource getBundle() {
		try {
			logger.info("Start loading property file.");
			resource = new ReloadableResourceBundleMessageSource();
			resource.setBasename(PATH);
			resource.setDefaultEncoding("UTF-8");
			logger.info("Property file loaded.");

		} catch (Exception e) {
			logger.debug("Error in loading roperty file", e);
			logger.error("Start loading property file.", e);
		}
		return resource;
	}

}
