package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnRoutine;

public interface GnRoutineRepo extends JpaRepository<GnRoutine, String> {

}
