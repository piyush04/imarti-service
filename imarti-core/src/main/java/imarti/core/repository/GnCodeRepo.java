package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnCode;

public interface GnCodeRepo extends JpaRepository<GnCode, String> {

}
