package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnShift;

public interface GnShiftRepo extends JpaRepository<GnShift, String> {

}
