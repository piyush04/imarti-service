package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnAddress;

public interface GnAddressRepo extends JpaRepository<GnAddress, String> {

}
