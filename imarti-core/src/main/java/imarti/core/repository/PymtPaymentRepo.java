package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.PymtPayment;

public interface PymtPaymentRepo extends JpaRepository<PymtPayment, String> {

}
