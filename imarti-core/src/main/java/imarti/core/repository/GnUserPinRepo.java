package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnUserPin;

public interface GnUserPinRepo extends JpaRepository<GnUserPin, String> {

}
