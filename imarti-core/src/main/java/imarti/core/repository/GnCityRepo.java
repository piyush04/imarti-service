package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnCity;

public interface GnCityRepo extends JpaRepository<GnCity, String> {

}
