package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnMenu;

public interface GnMenuRepo extends JpaRepository<GnMenu, String> {

}
