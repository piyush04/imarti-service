package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.PymtReceipt;

public interface PymtReceiptRepo extends JpaRepository<PymtReceipt, String> {

}
