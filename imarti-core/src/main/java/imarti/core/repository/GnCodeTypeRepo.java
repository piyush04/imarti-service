package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnCodeType;

public interface GnCodeTypeRepo extends JpaRepository<GnCodeType, String> {

}
