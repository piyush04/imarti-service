package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnState;

public interface GnStateRepo extends JpaRepository<GnState, String> {

}
