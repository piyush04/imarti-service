package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.GnCountry;

public interface GnCountryRepo extends JpaRepository<GnCountry, String> {

}
