package imarti.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import imarti.core.persist.PymtDiscount;

public interface PymtDiscountRepo extends JpaRepository<PymtDiscount, String> {

}
