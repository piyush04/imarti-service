package imarti.core.dao;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

@Service
@ComponentScan(basePackages = "imarti")
@EntityScan(basePackages = "imarti.core.persist")
@EnableJpaRepositories("imarti.core.repository")
public class RoutineDAO {

}
