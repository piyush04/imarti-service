package imarti.core.dao;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import imarti.core.persist.GnCountry;
import imarti.core.repository.GnCountryRepo;

@Service
@ComponentScan(basePackages = "imarti")
@EntityScan(basePackages = "imarti.core.persist")
@EnableJpaRepositories("imarti.core.repository")
public class CountryDAO {

	private static final Logger logger = LogManager.getLogger(CountryDAO.class);

	@Autowired
	GnCountryRepo gnCountryRepo;

	public void saveCountry(GnCountry gnCountry) {
		logger.info("Save Country : {}", gnCountry);
		gnCountryRepo.save(gnCountry);

	}

	public void deleteCountryByCtryId(String ctryId) {
		logger.info("Delete Country By Id : {}", ctryId);
		gnCountryRepo.deleteById(ctryId);

	}

	public Optional<GnCountry> getCountryByCtryId(String ctryId) {
		logger.info("Get Country By Id : {}", ctryId);
		return gnCountryRepo.findById(ctryId);

	}

}
