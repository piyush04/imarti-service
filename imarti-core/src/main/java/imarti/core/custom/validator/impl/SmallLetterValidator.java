package imarti.core.custom.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import imarti.core.custom.validator.annotation.SmallLetter;
import imarti.core.util.StringUtil;

/**
 * @author AbhinitKumar
 *
 */
public class SmallLetterValidator implements ConstraintValidator<SmallLetter, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null || value.isEmpty()) {
			return true;
		}

		return StringUtil.isCaptalLetter(value);
	}

}
