package imarti.core.custom.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import imarti.core.custom.validator.annotation.NoEmpty;

/**
 * @author AbhinitKumar
 *
 */
public class NoEmptyValidator implements ConstraintValidator<NoEmpty, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		return !value.isEmpty();
	}
}
