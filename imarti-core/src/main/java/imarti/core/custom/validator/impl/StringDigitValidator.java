package imarti.core.custom.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import imarti.core.custom.validator.annotation.StringDigit;
import imarti.core.util.RegexUtil;

public class StringDigitValidator implements ConstraintValidator<StringDigit, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null || value.isEmpty()) {
			return true;
		}
		return RegexUtil.isDigit(value);
	}

}
