package imarti.core.custom.validator.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import imarti.core.custom.validator.annotation.CapsLetter.List;
import imarti.core.custom.validator.impl.CapsLetterValidator;

/**
 * @author AbhinitKumar
 *
 *         <p>
 *         The annotated element letter must be in capital.
 *         </P>
 *
 *
 *         <p>
 *         Supported types are:
 *         <ul>
 *         <li>{@code CharSequence} (length of character sequence is
 *         evaluated)</li>
 *         <li>{@code Collection} (collection size is evaluated)</li>
 *         <li>{@code Map} (map size is evaluated)</li>
 *         <li>Array (array length is evaluated)</li>
 *         </ul>
 *         <p>
 *
 *         <b>Note: </b>{@code null} and {@code empty} elements are considered
 *         valid.
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Documented
@Repeatable(List.class)
@Constraint(validatedBy = { CapsLetterValidator.class })
public @interface CapsLetter {

	String message() default "${validatedValue} must be caps letter.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	public @interface List {
		CapsLetter[] value();
	}

}
