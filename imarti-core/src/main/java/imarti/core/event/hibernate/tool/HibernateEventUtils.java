package imarti.core.event.hibernate.tool;

import java.lang.reflect.Field;
import java.util.Arrays;

import org.hibernate.event.spi.AbstractPreDatabaseOperationEvent;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PreDeleteEvent;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.hibernate.persister.entity.EntityPersister;

/**
 * Utility class for working with Hibernate spi events.
 *
 * @author AbhinitKumar
 */
public class HibernateEventUtils {

	public static int getPropertyIndex(AbstractPreDatabaseOperationEvent event, String property) {
		return getPropertyIndex(event.getPersister(), property);
	}

	public static int getPropertyIndex(PreUpdateEvent event, String property) {
		return getPropertyIndex(event.getPersister(), property);
	}

	public static int getPropertyIndex(PreInsertEvent event, String property) {
		return getPropertyIndex(event.getPersister(), property);
	}

	public static int getPropertyIndex(PreDeleteEvent event, String property) {
		return getPropertyIndex(event.getPersister(), property);
	}

	public static int getPropertyIndex(PostUpdateEvent event, String property) {
		return getPropertyIndex(event.getPersister(), property);
	}

	public static int getPropertyIndex(PostInsertEvent event, String property) {
		return getPropertyIndex(event.getPersister(), property);
	}

	public static int getPropertyIndex(PostDeleteEvent event, String property) {
		return getPropertyIndex(event.getPersister(), property);
	}

	public static int getPropertyIndex(SaveOrUpdateEvent event, String property) {
		return getPropertyIndex(event.getEntry().getPersister(), property);
	}

	public static int getPropertyIndex(EntityPersister persister, String property) {
		return Arrays.asList(persister.getPropertyNames()).indexOf(property);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getAuditEntity(Object auditEntity, PreInsertEvent event) {
		Object aduitEntityNew = null;
		try {
			Field[] fields = auditEntity.getClass().getDeclaredFields();
			aduitEntityNew = auditEntity.getClass().newInstance();
			for (int i = 1; i < fields.length; i++) {
				Field field = fields[i];
				field.setAccessible(true);
				int index = getPropertyIndex(event, field.getName());
				if (index != -1) {
					Object obj = event.getState()[index];
					field.set(aduitEntityNew, obj);
				}
			}
		} catch (SecurityException | InstantiationException | IllegalAccessException e) {

		}
		return (T) aduitEntityNew;

	}

	@SuppressWarnings("unchecked")
	public static <T> T getAuditEntity(Object auditEntity, PreUpdateEvent event) {
		Object aduitEntityNew = null;
		try {
			Field[] fields = auditEntity.getClass().getDeclaredFields();
			aduitEntityNew = auditEntity.getClass().newInstance();
			for (int i = 1; i < fields.length; i++) {
				Field field = fields[i];
				field.setAccessible(true);
				int index = getPropertyIndex(event, field.getName());
				if (index != -1) {
					Object obj = event.getOldState()[index];
					field.set(aduitEntityNew, obj);
				}
			}
		} catch (SecurityException | InstantiationException | IllegalAccessException e) {

		}
		return (T) aduitEntityNew;

	}

	@SuppressWarnings("unchecked")
	public static <T> T getAuditEntity(Object auditEntity, PreDeleteEvent event) {
		Object aduitEntityNew = null;
		try {
			Field[] fields = auditEntity.getClass().getDeclaredFields();
			aduitEntityNew = auditEntity.getClass().newInstance();
			for (int i = 1; i < fields.length; i++) {
				Field field = fields[i];
				field.setAccessible(true);
				int index = getPropertyIndex(event, field.getName());
				if (index != -1) {
					Object obj = event.getDeletedState()[index];
					field.set(aduitEntityNew, obj);
				}
			}
		} catch (SecurityException | InstantiationException | IllegalAccessException e) {

		}
		return (T) aduitEntityNew;

	}

}
