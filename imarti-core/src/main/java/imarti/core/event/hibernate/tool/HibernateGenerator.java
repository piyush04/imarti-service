package imarti.core.event.hibernate.tool;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.GenerationType;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;

/**
 * 
 * Generates identifier values based on a sequence-style database structure.
 * Support with only {@link GenerationType #IDENTITY} Variations range from
 * actually using a sequence to using a table to mimic a sequence.
 * 
 * 
 * <p/>
 * <b>NOTE</b> that by default we utilize a single database sequence for all
 * generators. The configuration parameter {@link #PRMIARY_COLUMN_NAME} and
 * {@link #TABLE_NAME} Starting value of primary key
 * {@link #INITIALIZE_PRIMARY_KEY}.
 * <p/>
 * 
 * @author AbhinitKumar
 *
 */
public class HibernateGenerator extends SequenceStyleGenerator {

	public static final String PRMIARY_COLUMN_NAME = "valuePrefix";
	private static final String VALUE_PREFIX_DEFAULT = "";
	private String primaryColumeName;
	public static final String TABLE_NAME = "numberFormat";
	public static final String NUMBER_FORMAT_DEFAULT = "%d";
	private String tableName;
	private static final String INITIALIZE_PRIMARY_KEY = "1000000";

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		String primaryKey = null;
		try {
			String sqlQuery = "SELECT max(Convert(`" + primaryColumeName + "`, SIGNED)) as primaryKey FROM " + tableName
					+ "";
			Connection connection = session.connection();
			PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
			ResultSet resultSet = preparedStatement.executeQuery();

			resultSet.next();
			primaryKey = resultSet.getString(1);
			primaryKey = primaryKey == null ? INITIALIZE_PRIMARY_KEY : primaryKey;
			return new BigInteger(primaryKey).add(BigInteger.ONE).toString();

		} catch (SQLException se) {
			throw new HibernateException("Primary key sequence generator error", se);
		}

	}

	@Override
	public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
		super.configure(LongType.INSTANCE, params, serviceRegistry);
		primaryColumeName = ConfigurationHelper.getString(PRMIARY_COLUMN_NAME, params, VALUE_PREFIX_DEFAULT);
		tableName = ConfigurationHelper.getString(TABLE_NAME, params, NUMBER_FORMAT_DEFAULT);
	}

}
