package imarti.core.event.hibernate.exception;

/**
 * Exception for when a method has in incorrect number of parameters defined.
 *
 * @author Tyler Eastman
 */
public class InvalidParameterCount extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidParameterCount() {
	}

	public InvalidParameterCount(String message) {
		super(message);
	}
}
