package imarti.core.event.hibernate.exception;

/**
 * Exception for when a method parameter does not inherit from a defined type.
 *
 * @author AbhinitKumar
 */
public class AssignableParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AssignableParameterException() {
	}

	public AssignableParameterException(String message) {
		super(message);
	}
}
