package imarti.core.event.hibernate.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that marks a method as a listener for hibernate spi events.
 *
 * <p>
 * The first parameter of the annotated method must be the entity that hibernate
 * affected. The second parameter must be of type
 * org.hibernate.event.spi.AbstractEvent
 *
 * <p>
 * It is also possible to use Spring's
 * {@link org.springframework.core.annotation.Order @Order} annotation to define
 * the order in which listeners for a given event type are invoked.
 *
 * @author AbhinitKumar
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HibernateEventListener {
}
