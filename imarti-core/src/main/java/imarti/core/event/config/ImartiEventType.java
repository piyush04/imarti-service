package imarti.core.event.config;

/**
 * @author AbhinitKumar
 *
 */
public final class ImartiEventType {

	private ImartiEventType() {
		super();
	}

	public static final String POST_INSERT = "POST_INSERT";
	public static final String PRE_INSERT = "PRE_INSERT";
	public static final String PRE_UPDATE = "PRE_UPDATE";
	public static final String POST_UPDATE = "POST_UPDATE";
	public static final String PRE_DELETE = "PRE_DELETE";
	public static final String POST_DELETE = "POST_DELETE";
	public static final String PRE_COLLECTION_RECREATE = "PRE_COLLECTION_RECREATE";
	public static final String PRE_COLLECTION_REMOVE = "PRE_COLLECTION_REMOVE";
	public static final String PRE_COLLECTION_UPDATE = "PRE_COLLECTION_UPDATE";
	public static final String POST_COLLECTION_RECREATE = "POST_COLLECTION_RECREATE";
	public static final String POST_COLLECTION_REMOVE = "POST_COLLECTION_REMOVE";
	public static final String POST_COLLECTION_UPDATE = "POST_COLLECTION_UPDATE";
	public static final String SAVE_UPDATE = "SAVE_UPDATE";
	public static final String SAVE = "SAVE";
	public static final String USER = "User";

}
