package imarti.core.message;

/**
 * Interface representing a message code and its operations.
 */
public interface MessageCode extends ServiceCode {
}
