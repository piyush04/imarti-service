package imarti.core.message;

import java.util.MissingResourceException;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Helper to retrieve messages in bundles.
 */
public final class ServiceCodeHelper {

	/**
	 * Message displayed as the exception message when the pair fault code / message
	 * cannot be found in the related bundle.
	 */
	private static final String CODE_MISSING_MSG = "Pair (Service code/message) missing in related bundle. Please provide a suitable message for the code : ";

	/**
	 * Private constructor.
	 */
	private ServiceCodeHelper() {

	}

	/**
	 * Return the message associated to the code.
	 * 
	 * @param serviceCode the code
	 * @return the message associated
	 */
	public static String getMessage(final ServiceCode serviceCode, final Object... messageArguments) {
		if (serviceCode == null) {
			return null;
		}
		if (serviceCode instanceof FaultCodeWithMessage) {
			return ((FaultCodeWithMessage) serviceCode).getFormattedMessage();
		}
		String key = serviceCode.getKey();
		try {
			return serviceCode.getBundle().getMessage(key, messageArguments, LocaleContextHolder.getLocale());
		} catch (MissingResourceException e) {
			return "! " + CODE_MISSING_MSG + serviceCode.getClass().getSimpleName() + "." + key + " !";
		}
	}

}
