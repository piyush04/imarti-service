package imarti.core.message;

import java.io.Serializable;

/**
 * Created by AbhinitKumar.
 *
 * Helper class for passing FaultCode with parameters necessary to display
 * message from method
 */
public class FaultCodeWithParameters implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private FaultCode faultCode;
	private Object[] parameters;

	/**
	 * constructors;
	 */
	public FaultCodeWithParameters(FaultCode faultCode, Object... parameters) {
		this.faultCode = faultCode;
		this.parameters = parameters;
	}

	public FaultCodeWithParameters(FaultCode faultCode) {
		this.faultCode = faultCode;
		this.parameters = new Object[0];
	}

	/**
	 * FaultCode/ Parameters.
	 * 
	 */
	public FaultCode getFaultCode() {
		return faultCode;
	}

	public Object[] getParameters() {
		return parameters;
	}

}
