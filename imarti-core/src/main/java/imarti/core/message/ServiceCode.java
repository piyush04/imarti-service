package imarti.core.message;

import java.io.Serializable;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;


/**
 * Interface representing a code and its operations.
 */
public interface ServiceCode extends Serializable{

    String getKey();

    ReloadableResourceBundleMessageSource getBundle();
}
