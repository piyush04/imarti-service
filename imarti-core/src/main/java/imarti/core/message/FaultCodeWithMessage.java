package imarti.core.message;

import java.io.Serializable;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Allow the transfer of an existing FaultCode where the message has already
 * been formatted previously and we do not have access to its bundle anymore.
 */
class FaultCodeWithMessage implements FaultCode, Serializable {

	private static final long serialVersionUID = 1L;

	private final String key;
	private final String formattedMessage;

	public FaultCodeWithMessage(FaultCode faultCode, String formattedMessage) {
		this.key = faultCode.getKey();
		this.formattedMessage = formattedMessage;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	public String getFormattedMessage() {
		return this.formattedMessage;
	}

	@Override
	public ReloadableResourceBundleMessageSource getBundle() {
		throw new IllegalStateException("Bundle should not be accessed as message has already been formatted.");
	}

}
