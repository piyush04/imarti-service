package imarti.core.message;

/**
 * Interface representing a fault code and its operations.
 */
public interface FaultCode extends ServiceCode {
}
