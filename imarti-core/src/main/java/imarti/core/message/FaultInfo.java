package imarti.core.message;

import java.io.Serializable;

/**
 * 
 * Disclaimer: some element names appear to be slightly awkward but were left
 * uncorrected for backward compatibility, which is not a valid reason for
 * making complaints later to the author of this class. :)
 * 
 * @author AbhinitKumar
 * 
 */

public class FaultInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String errorClass;
	private String errorMessage;
	private String faultCode;
	private String message;

	public String getErrorClass() {
		return errorClass;
	}

	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getFaultCode() {
		return faultCode;
	}

	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
