package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_address database table.
 * 
 */
@Entity
@Table(name = "gn_address")
@NamedQuery(name = "GnAddress.findAll", query = "SELECT g FROM GnAddress g")
public class GnAddress implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_address")
	@GenericGenerator(name = "gn_address", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_address"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "adrs_id") })
	@Column(name = "adrs_id")
	private String adrsId;

	@Column(name = "adrs_address_line_1")
	private String adrsAddressLine1;

	@Column(name = "adrs_address_line_2")
	private String adrsAddressLine2;

	@Column(name = "adrs_address_line_3")
	private String adrsAddressLine3;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "adrs_last_updated")
	private Date adrsLastUpdated;

	@Column(name = "adrs_last_updated_by")
	private String adrsLastUpdatedBy;

	@Column(name = "adrs_pin_code")
	private String adrsPinCode;

	@Column(name = "adrs_timestamp")
	private int adrsTimestamp;

	// bi-directional many-to-one association to GnCity
	@ManyToOne
	@JoinColumn(name = "adrs_city_id")
	private GnCity gnCity;

	// bi-directional many-to-one association to GnCountry
	@ManyToOne
	@JoinColumn(name = "adrs_country_id")
	private GnCountry gnCountry;

	// bi-directional many-to-one association to GnState
	@ManyToOne
	@JoinColumn(name = "adrs_state_id")
	private GnState gnState;

	// bi-directional many-to-one association to GnUser
	@ManyToOne
	@JoinColumn(name = "adrs_user_id")
	private GnUser gnUser;

	public GnAddress() {
	}

	public String getAdrsId() {
		return this.adrsId;
	}

	public void setAdrsId(String adrsId) {
		this.adrsId = adrsId;
	}

	public String getAdrsAddressLine1() {
		return this.adrsAddressLine1;
	}

	public void setAdrsAddressLine1(String adrsAddressLine1) {
		this.adrsAddressLine1 = adrsAddressLine1;
	}

	public String getAdrsAddressLine2() {
		return this.adrsAddressLine2;
	}

	public void setAdrsAddressLine2(String adrsAddressLine2) {
		this.adrsAddressLine2 = adrsAddressLine2;
	}

	public String getAdrsAddressLine3() {
		return this.adrsAddressLine3;
	}

	public void setAdrsAddressLine3(String adrsAddressLine3) {
		this.adrsAddressLine3 = adrsAddressLine3;
	}

	public Date getAdrsLastUpdated() {
		return this.adrsLastUpdated;
	}

	public void setAdrsLastUpdated(Date adrsLastUpdated) {
		this.adrsLastUpdated = adrsLastUpdated;
	}

	public String getAdrsLastUpdatedBy() {
		return this.adrsLastUpdatedBy;
	}

	public void setAdrsLastUpdatedBy(String adrsLastUpdatedBy) {
		this.adrsLastUpdatedBy = adrsLastUpdatedBy;
	}

	public String getAdrsPinCode() {
		return this.adrsPinCode;
	}

	public void setAdrsPinCode(String adrsPinCode) {
		this.adrsPinCode = adrsPinCode;
	}

	public int getAdrsTimestamp() {
		return this.adrsTimestamp;
	}

	public void setAdrsTimestamp(int adrsTimestamp) {
		this.adrsTimestamp = adrsTimestamp;
	}

	public GnCity getGnCity() {
		return this.gnCity;
	}

	public void setGnCity(GnCity gnCity) {
		this.gnCity = gnCity;
	}

	public GnCountry getGnCountry() {
		return this.gnCountry;
	}

	public void setGnCountry(GnCountry gnCountry) {
		this.gnCountry = gnCountry;
	}

	public GnState getGnState() {
		return this.gnState;
	}

	public void setGnState(GnState gnState) {
		this.gnState = gnState;
	}

	public GnUser getGnUser() {
		return this.gnUser;
	}

	public void setGnUser(GnUser gnUser) {
		this.gnUser = gnUser;
	}

}