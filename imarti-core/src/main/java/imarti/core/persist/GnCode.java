package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_code database table.
 * 
 */
@Entity
@Table(name = "gn_code")
@NamedQuery(name = "GnCode.findAll", query = "SELECT g FROM GnCode g")
public class GnCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_code")
	@GenericGenerator(name = "gn_code", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_code"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "code_id") })
	@Column(name = "code_id")
	private String codeId;

	@Column(name = "code_code")
	private String codeCode;

	@Column(name = "code_description")
	private String codeDescription;

	@Column(name = "code_display")
	private String codeDisplay;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "code_last_updated")
	private Date codeLastUpdated;

	@Column(name = "code_last_updated_by")
	private String codeLastUpdatedBy;

	@Column(name = "code_short_description")
	private String codeShortDescription;

	@Column(name = "code_timestamp")
	private int codeTimestamp;

	// bi-directional many-to-one association to GnCodeType
	@ManyToOne
	@JoinColumn(name = "code_type_id")
	private GnCodeType gnCodeType;

	public GnCode() {
	}

	public String getCodeId() {
		return this.codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getCodeCode() {
		return this.codeCode;
	}

	public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	}

	public String getCodeDescription() {
		return this.codeDescription;
	}

	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}

	public String getCodeDisplay() {
		return this.codeDisplay;
	}

	public void setCodeDisplay(String codeDisplay) {
		this.codeDisplay = codeDisplay;
	}

	public Date getCodeLastUpdated() {
		return this.codeLastUpdated;
	}

	public void setCodeLastUpdated(Date codeLastUpdated) {
		this.codeLastUpdated = codeLastUpdated;
	}

	public String getCodeLastUpdatedBy() {
		return this.codeLastUpdatedBy;
	}

	public void setCodeLastUpdatedBy(String codeLastUpdatedBy) {
		this.codeLastUpdatedBy = codeLastUpdatedBy;
	}

	public String getCodeShortDescription() {
		return this.codeShortDescription;
	}

	public void setCodeShortDescription(String codeShortDescription) {
		this.codeShortDescription = codeShortDescription;
	}

	public int getCodeTimestamp() {
		return this.codeTimestamp;
	}

	public void setCodeTimestamp(int codeTimestamp) {
		this.codeTimestamp = codeTimestamp;
	}

	public GnCodeType getGnCodeType() {
		return this.gnCodeType;
	}

	public void setGnCodeType(GnCodeType gnCodeType) {
		this.gnCodeType = gnCodeType;
	}

}