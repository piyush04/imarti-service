package imarti.core.persist;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;


/**
 * The persistent class for the gn_shift database table.
 * 
 */
@Entity
@Table(name="gn_shift")
@NamedQuery(name="GnShift.findAll", query="SELECT g FROM GnShift g")
public class GnShift implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_shift")
	@GenericGenerator(name = "gn_shift", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_shift"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "shft_id") })
	@Column(name="shft_id")
	private String shftId;

	public GnShift() {
	}

	public String getShftId() {
		return this.shftId;
	}

	public void setShftId(String shftId) {
		this.shftId = shftId;
	}

}