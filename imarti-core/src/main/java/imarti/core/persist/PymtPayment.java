package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the pymt_payment database table.
 * 
 */
@Entity
@Table(name = "pymt_payment")
@NamedQuery(name = "PymtPayment.findAll", query = "SELECT p FROM PymtPayment p")
public class PymtPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "pymt_payment")
	@GenericGenerator(name = "pymt_payment", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "pymt_payment"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "paym_id") })
	@Column(name = "paym_id")
	private String paymId;

	@Column(name = "paym_charge_remark")
	private String paymChargeRemark;

	@Column(name = "paym_due_amount")
	private String paymDueAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "paym_last_updated")
	private Date paymLastUpdated;

	@Column(name = "paym_last_updated_by")
	private String paymLastUpdatedBy;

	@Column(name = "paym_timestamp")
	private int paymTimestamp;

	@Column(name = "paym_total_amount")
	private String paymTotalAmount;

	// bi-directional many-to-one association to GnCode
	@ManyToOne
	@JoinColumn(name = "paym_type_code_id")
	private GnCode gnCode;

	// bi-directional many-to-one association to GnUser
	@ManyToOne
	@JoinColumn(name = "paym_user_id")
	private GnUser gnUser;

	// bi-directional many-to-one association to PymtDiscount
	@ManyToOne
	@JoinColumn(name = "paym_discount_id")
	private PymtDiscount pymtDiscount;

	public PymtPayment() {
	}

	public String getPaymId() {
		return this.paymId;
	}

	public void setPaymId(String paymId) {
		this.paymId = paymId;
	}

	public String getPaymChargeRemark() {
		return this.paymChargeRemark;
	}

	public void setPaymChargeRemark(String paymChargeRemark) {
		this.paymChargeRemark = paymChargeRemark;
	}

	public String getPaymDueAmount() {
		return this.paymDueAmount;
	}

	public void setPaymDueAmount(String paymDueAmount) {
		this.paymDueAmount = paymDueAmount;
	}

	public Date getPaymLastUpdated() {
		return this.paymLastUpdated;
	}

	public void setPaymLastUpdated(Date paymLastUpdated) {
		this.paymLastUpdated = paymLastUpdated;
	}

	public String getPaymLastUpdatedBy() {
		return this.paymLastUpdatedBy;
	}

	public void setPaymLastUpdatedBy(String paymLastUpdatedBy) {
		this.paymLastUpdatedBy = paymLastUpdatedBy;
	}

	public int getPaymTimestamp() {
		return this.paymTimestamp;
	}

	public void setPaymTimestamp(int paymTimestamp) {
		this.paymTimestamp = paymTimestamp;
	}

	public String getPaymTotalAmount() {
		return this.paymTotalAmount;
	}

	public void setPaymTotalAmount(String paymTotalAmount) {
		this.paymTotalAmount = paymTotalAmount;
	}

	public GnCode getGnCode() {
		return this.gnCode;
	}

	public void setGnCode(GnCode gnCode) {
		this.gnCode = gnCode;
	}

	public GnUser getGnUser() {
		return this.gnUser;
	}

	public void setGnUser(GnUser gnUser) {
		this.gnUser = gnUser;
	}

	public PymtDiscount getPymtDiscount() {
		return this.pymtDiscount;
	}

	public void setPymtDiscount(PymtDiscount pymtDiscount) {
		this.pymtDiscount = pymtDiscount;
	}

}