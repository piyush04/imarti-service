package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the pymt_discount database table.
 * 
 */
@Entity
@Table(name = "pymt_discount")
@NamedQuery(name = "PymtDiscount.findAll", query = "SELECT p FROM PymtDiscount p")
public class PymtDiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "pymt_discount")
	@GenericGenerator(name = "pymt_discount", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "pymt_discount"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "dcnt_id") })
	@Column(name = "dcnt_id")
	private String dcntId;

	@Column(name = "dcnt_code")
	private String dcntCode;

	@Column(name = "dcnt_descripton")
	private String dcntDescripton;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dcnt_end_date")
	private Date dcntEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dcnt_last_updated")
	private Date dcntLastUpdated;

	@Column(name = "dcnt_last_updated_by")
	private String dcntLastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dcnt_start_date")
	private Date dcntStartDate;

	@Column(name = "dcnt_timestamp")
	private int dcntTimestamp;

	// bi-directional many-to-one association to GnCode
	@ManyToOne
	@JoinColumn(name = "dcnt_status_code_id")
	private GnCode gnCode;

	public PymtDiscount() {
	}

	public String getDcntId() {
		return this.dcntId;
	}

	public void setDcntId(String dcntId) {
		this.dcntId = dcntId;
	}

	public String getDcntCode() {
		return this.dcntCode;
	}

	public void setDcntCode(String dcntCode) {
		this.dcntCode = dcntCode;
	}

	public String getDcntDescripton() {
		return this.dcntDescripton;
	}

	public void setDcntDescripton(String dcntDescripton) {
		this.dcntDescripton = dcntDescripton;
	}

	public Date getDcntEndDate() {
		return this.dcntEndDate;
	}

	public void setDcntEndDate(Date dcntEndDate) {
		this.dcntEndDate = dcntEndDate;
	}

	public Date getDcntLastUpdated() {
		return this.dcntLastUpdated;
	}

	public void setDcntLastUpdated(Date dcntLastUpdated) {
		this.dcntLastUpdated = dcntLastUpdated;
	}

	public String getDcntLastUpdatedBy() {
		return this.dcntLastUpdatedBy;
	}

	public void setDcntLastUpdatedBy(String dcntLastUpdatedBy) {
		this.dcntLastUpdatedBy = dcntLastUpdatedBy;
	}

	public Date getDcntStartDate() {
		return this.dcntStartDate;
	}

	public void setDcntStartDate(Date dcntStartDate) {
		this.dcntStartDate = dcntStartDate;
	}

	public int getDcntTimestamp() {
		return this.dcntTimestamp;
	}

	public void setDcntTimestamp(int dcntTimestamp) {
		this.dcntTimestamp = dcntTimestamp;
	}

	public GnCode getGnCode() {
		return this.gnCode;
	}

	public void setGnCode(GnCode gnCode) {
		this.gnCode = gnCode;
	}

}