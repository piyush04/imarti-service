package imarti.core.persist;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;


/**
 * The persistent class for the gn_menu database table.
 * 
 */
@Entity
@Table(name="gn_menu")
@NamedQuery(name="GnMenu.findAll", query="SELECT g FROM GnMenu g")
public class GnMenu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_menu")
	@GenericGenerator(name = "gn_menu", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_menu"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "menu_id") })
	@Column(name="menu_id")
	private String menuId;

	public GnMenu() {
	}

	public String getMenuId() {
		return this.menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

}