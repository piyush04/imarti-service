package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the pymt_receipt database table.
 * 
 */
@Entity
@Table(name = "pymt_receipt")
@NamedQuery(name = "PymtReceipt.findAll", query = "SELECT p FROM PymtReceipt p")
public class PymtReceipt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "pymt_receipt")
	@GenericGenerator(name = "pymt_receipt", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "pymt_receipt"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "rcpt_id") })
	@Column(name = "rcpt_id")
	private String rcptId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "rcpt_date")
	private Date rcptDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "rcpt_last_updated")
	private Date rcptLastUpdated;

	@Column(name = "rcpt_last_updated_by")
	private String rcptLastUpdatedBy;

	@Column(name = "rcpt_notes")
	private String rcptNotes;

	@Column(name = "rcpt_payer")
	private String rcptPayer;

	@Column(name = "rcpt_printed")
	private String rcptPrinted;

	@Column(name = "rcpt_receipt_number")
	private String rcptReceiptNumber;

	@Column(name = "rcpt_timestamp")
	private int rcptTimestamp;

	// bi-directional many-to-one association to GnCode
	@ManyToOne
	@JoinColumn(name = "rcpt_status_code_id")
	private GnCode gnCode;

	// bi-directional many-to-one association to GnUser
	@ManyToOne
	@JoinColumn(name = "rcpt_user_id")
	private GnUser gnUser;

	// bi-directional many-to-one association to PymtPayment
	@ManyToOne
	@JoinColumn(name = "rcpt_payment_id")
	private PymtPayment pymtPayment;

	public PymtReceipt() {
	}

	public String getRcptId() {
		return this.rcptId;
	}

	public void setRcptId(String rcptId) {
		this.rcptId = rcptId;
	}

	public Date getRcptDate() {
		return this.rcptDate;
	}

	public void setRcptDate(Date rcptDate) {
		this.rcptDate = rcptDate;
	}

	public Date getRcptLastUpdated() {
		return this.rcptLastUpdated;
	}

	public void setRcptLastUpdated(Date rcptLastUpdated) {
		this.rcptLastUpdated = rcptLastUpdated;
	}

	public String getRcptLastUpdatedBy() {
		return this.rcptLastUpdatedBy;
	}

	public void setRcptLastUpdatedBy(String rcptLastUpdatedBy) {
		this.rcptLastUpdatedBy = rcptLastUpdatedBy;
	}

	public String getRcptNotes() {
		return this.rcptNotes;
	}

	public void setRcptNotes(String rcptNotes) {
		this.rcptNotes = rcptNotes;
	}

	public String getRcptPayer() {
		return this.rcptPayer;
	}

	public void setRcptPayer(String rcptPayer) {
		this.rcptPayer = rcptPayer;
	}

	public String getRcptPrinted() {
		return this.rcptPrinted;
	}

	public void setRcptPrinted(String rcptPrinted) {
		this.rcptPrinted = rcptPrinted;
	}

	public String getRcptReceiptNumber() {
		return this.rcptReceiptNumber;
	}

	public void setRcptReceiptNumber(String rcptReceiptNumber) {
		this.rcptReceiptNumber = rcptReceiptNumber;
	}

	public int getRcptTimestamp() {
		return this.rcptTimestamp;
	}

	public void setRcptTimestamp(int rcptTimestamp) {
		this.rcptTimestamp = rcptTimestamp;
	}

	public GnCode getGnCode() {
		return this.gnCode;
	}

	public void setGnCode(GnCode gnCode) {
		this.gnCode = gnCode;
	}

	public GnUser getGnUser() {
		return this.gnUser;
	}

	public void setGnUser(GnUser gnUser) {
		this.gnUser = gnUser;
	}

	public PymtPayment getPymtPayment() {
		return this.pymtPayment;
	}

	public void setPymtPayment(PymtPayment pymtPayment) {
		this.pymtPayment = pymtPayment;
	}

}