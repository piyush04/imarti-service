package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_user database table.
 * 
 */
@Entity
@Table(name = "gn_user")
@NamedQuery(name = "GnUser.findAll", query = "SELECT g FROM GnUser g")
public class GnUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_user")
	@GenericGenerator(name = "gn_user", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_user"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "user_id") })
	@Column(name = "user_id")
	private String userId;

	@Column(name = "user_date_of_birth")
	private String userDateOfBirth;

	@Column(name = "user_email")
	private String userEmail;

	@Column(name = "user_frst_name")
	private String userFrstName;

	@Column(name = "user_last_name")
	private String userLastName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "user_last_updated")
	private Date userLastUpdated;

	@Column(name = "user_last_updated_by")
	private String userLastUpdatedBy;

	@Column(name = "user_login")
	private String userLogin;

	@Column(name = "user_middle_name")
	private String userMiddleName;

	@Column(name = "user_timestamp")
	private int userTimestamp;

	@Column(name = "`user_mobile_number`")
	private String userMobileNumber;

	// bi-directional many-to-one association to GnCode
	@ManyToOne
	@JoinColumn(name = "user_status_code_id")
	private GnCode gnCode1;

	// bi-directional many-to-one association to GnCode
	@ManyToOne
	@JoinColumn(name = "user_title_code_id")
	private GnCode gnCode2;

	// bi-directional many-to-one association to GnUserPin
	@ManyToOne
	@JoinColumn(name = "user_pin_id")
	private GnUserPin gnUserPin;

	public GnUser() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserDateOfBirth() {
		return this.userDateOfBirth;
	}

	public void setUserDateOfBirth(String userDateOfBirth) {
		this.userDateOfBirth = userDateOfBirth;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserFrstName() {
		return this.userFrstName;
	}

	public void setUserFrstName(String userFrstName) {
		this.userFrstName = userFrstName;
	}

	public String getUserLastName() {
		return this.userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public Date getUserLastUpdated() {
		return this.userLastUpdated;
	}

	public void setUserLastUpdated(Date userLastUpdated) {
		this.userLastUpdated = userLastUpdated;
	}

	public String getUserLastUpdatedBy() {
		return this.userLastUpdatedBy;
	}

	public void setUserLastUpdatedBy(String userLastUpdatedBy) {
		this.userLastUpdatedBy = userLastUpdatedBy;
	}

	public String getUserLogin() {
		return this.userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getUserMiddleName() {
		return this.userMiddleName;
	}

	public void setUserMiddleName(String userMiddleName) {
		this.userMiddleName = userMiddleName;
	}

	public int getUserTimestamp() {
		return this.userTimestamp;
	}

	public void setUserTimestamp(int userTimestamp) {
		this.userTimestamp = userTimestamp;
	}

	public String getuserMobileNumber() {
		return this.userMobileNumber;
	}

	public void setuserMobileNumber(String userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}

	public GnCode getGnCode1() {
		return this.gnCode1;
	}

	public void setGnCode1(GnCode gnCode1) {
		this.gnCode1 = gnCode1;
	}

	public GnCode getGnCode2() {
		return this.gnCode2;
	}

	public void setGnCode2(GnCode gnCode2) {
		this.gnCode2 = gnCode2;
	}

	public GnUserPin getGnUserPin() {
		return this.gnUserPin;
	}

	public void setGnUserPin(GnUserPin gnUserPin) {
		this.gnUserPin = gnUserPin;
	}

}