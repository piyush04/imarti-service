package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_user_pin database table.
 * 
 */
@Entity
@Table(name = "gn_user_pin")
@NamedQuery(name = "GnUserPin.findAll", query = "SELECT g FROM GnUserPin g")
public class GnUserPin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_user_pin")
	@GenericGenerator(name = "gn_user_pin", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_user_pin"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "upin_id") })
	@Column(name = "upin_id")
	private String upinId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upin_creation_date")
	private Date upinCreationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upin_effective_date")
	private Date upinEffectiveDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upin_expiry_date")
	private Date upinExpiryDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upin_last_login_date")
	private Date upinLastLoginDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upin_last_updated")
	private Date upinLastUpdated;

	@Column(name = "upin_last_updated_by")
	private String upinLastUpdatedBy;

	@Column(name = "upin_login_count")
	private int upinLoginCount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "upin_logout_period_end")
	private Date upinLogoutPeriodEnd;

	@Column(name = "upin_pin_number")
	private String upinPinNumber;

	@Column(name = "upin_recipe")
	private String upinRecipe;

	@Column(name = "upin_registration_number")
	private String upinRegistrationNumber;

	@Column(name = "upin_salt")
	private String upinSalt;

	@Column(name = "upin_timestamp")
	private int upinTimestamp;

	// bi-directional many-to-one association to GnCode
	@ManyToOne
	@JoinColumn(name = "upin_status_code_id")
	private GnCode gnCode1;

	// bi-directional many-to-one association to GnCode
	@ManyToOne
	@JoinColumn(name = "upin_type_code_id")
	private GnCode gnCode2;

	// bi-directional many-to-one association to GnUser
	@ManyToOne
	@JoinColumn(name = "upin_user_id")
	private GnUser gnUser;

	public GnUserPin() {
	}

	public String getUpinId() {
		return this.upinId;
	}

	public void setUpinId(String upinId) {
		this.upinId = upinId;
	}

	public Date getUpinCreationDate() {
		return this.upinCreationDate;
	}

	public void setUpinCreationDate(Date upinCreationDate) {
		this.upinCreationDate = upinCreationDate;
	}

	public Date getUpinEffectiveDate() {
		return this.upinEffectiveDate;
	}

	public void setUpinEffectiveDate(Date upinEffectiveDate) {
		this.upinEffectiveDate = upinEffectiveDate;
	}

	public Date getUpinExpiryDate() {
		return this.upinExpiryDate;
	}

	public void setUpinExpiryDate(Date upinExpiryDate) {
		this.upinExpiryDate = upinExpiryDate;
	}

	public Date getUpinLastLoginDate() {
		return this.upinLastLoginDate;
	}

	public void setUpinLastLoginDate(Date upinLastLoginDate) {
		this.upinLastLoginDate = upinLastLoginDate;
	}

	public Date getUpinLastUpdated() {
		return this.upinLastUpdated;
	}

	public void setUpinLastUpdated(Date upinLastUpdated) {
		this.upinLastUpdated = upinLastUpdated;
	}

	public String getUpinLastUpdatedBy() {
		return this.upinLastUpdatedBy;
	}

	public void setUpinLastUpdatedBy(String upinLastUpdatedBy) {
		this.upinLastUpdatedBy = upinLastUpdatedBy;
	}

	public int getUpinLoginCount() {
		return this.upinLoginCount;
	}

	public void setUpinLoginCount(int upinLoginCount) {
		this.upinLoginCount = upinLoginCount;
	}

	public Date getUpinLogoutPeriodEnd() {
		return this.upinLogoutPeriodEnd;
	}

	public void setUpinLogoutPeriodEnd(Date upinLogoutPeriodEnd) {
		this.upinLogoutPeriodEnd = upinLogoutPeriodEnd;
	}

	public String getUpinPinNumber() {
		return this.upinPinNumber;
	}

	public void setUpinPinNumber(String upinPinNumber) {
		this.upinPinNumber = upinPinNumber;
	}

	public String getUpinRecipe() {
		return this.upinRecipe;
	}

	public void setUpinRecipe(String upinRecipe) {
		this.upinRecipe = upinRecipe;
	}

	public String getUpinRegistrationNumber() {
		return this.upinRegistrationNumber;
	}

	public void setUpinRegistrationNumber(String upinRegistrationNumber) {
		this.upinRegistrationNumber = upinRegistrationNumber;
	}

	public String getUpinSalt() {
		return this.upinSalt;
	}

	public void setUpinSalt(String upinSalt) {
		this.upinSalt = upinSalt;
	}

	public int getUpinTimestamp() {
		return this.upinTimestamp;
	}

	public void setUpinTimestamp(int upinTimestamp) {
		this.upinTimestamp = upinTimestamp;
	}

	public GnCode getGnCode1() {
		return this.gnCode1;
	}

	public void setGnCode1(GnCode gnCode1) {
		this.gnCode1 = gnCode1;
	}

	public GnCode getGnCode2() {
		return this.gnCode2;
	}

	public void setGnCode2(GnCode gnCode2) {
		this.gnCode2 = gnCode2;
	}

	public GnUser getGnUser() {
		return this.gnUser;
	}

	public void setGnUser(GnUser gnUser) {
		this.gnUser = gnUser;
	}

}