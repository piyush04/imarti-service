package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_city database table.
 * 
 */
@Entity
@Table(name = "gn_city")
@NamedQuery(name = "GnCity.findAll", query = "SELECT g FROM GnCity g")
public class GnCity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_city")
	@GenericGenerator(name = "gn_city", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_city"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "city_id") })
	@Column(name = "city_id")
	private String cityId;

	@Column(name = "city_code")
	private String cityCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "city_last_updated")
	private Date cityLastUpdated;

	@Column(name = "city_last_updated_by")
	private String cityLastUpdatedBy;

	@Column(name = "city_name")
	private String cityName;

	@Column(name = "city_timestamp")
	private int cityTimestamp;

	// bi-directional many-to-one association to GnState
	@ManyToOne
	@JoinColumn(name = "city_state_id")
	private GnState gnState;

	public GnCity() {
	}

	public String getCityId() {
		return this.cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public Date getCityLastUpdated() {
		return this.cityLastUpdated;
	}

	public void setCityLastUpdated(Date cityLastUpdated) {
		this.cityLastUpdated = cityLastUpdated;
	}

	public String getCityLastUpdatedBy() {
		return this.cityLastUpdatedBy;
	}

	public void setCityLastUpdatedBy(String cityLastUpdatedBy) {
		this.cityLastUpdatedBy = cityLastUpdatedBy;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public int getCityTimestamp() {
		return this.cityTimestamp;
	}

	public void setCityTimestamp(int cityTimestamp) {
		this.cityTimestamp = cityTimestamp;
	}

	public GnState getGnState() {
		return this.gnState;
	}

	public void setGnState(GnState gnState) {
		this.gnState = gnState;
	}

}