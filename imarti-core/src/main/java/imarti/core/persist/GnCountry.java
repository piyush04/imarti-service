package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_country database table.
 * 
 */
@Entity
@Table(name = "gn_country")
@NamedQuery(name = "GnCountry.findAll", query = "SELECT g FROM GnCountry g")
public class GnCountry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_country")
	@GenericGenerator(name = "gn_country", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_country"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "ctry_id") })
	@Column(name = "ctry_id")
	private String ctryId;

	@Column(name = "ctry_code")
	private String ctryCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ctry_last_updated")
	private Date ctryLastUpdated;

	@Column(name = "ctry_last_updated_by")
	private String ctryLastUpdatedBy;

	@Column(name = "ctry_name")
	private String ctryName;

	@Column(name = "ctry_timestamp")
	private int ctryTimestamp;

	public GnCountry() {
	}

	public String getCtryId() {
		return this.ctryId;
	}

	public void setCtryId(String ctryId) {
		this.ctryId = ctryId;
	}

	public String getCtryCode() {
		return this.ctryCode;
	}

	public void setCtryCode(String ctryCode) {
		this.ctryCode = ctryCode;
	}

	public Date getCtryLastUpdated() {
		return this.ctryLastUpdated;
	}

	public void setCtryLastUpdated(Date ctryLastUpdated) {
		this.ctryLastUpdated = ctryLastUpdated;
	}

	public String getCtryLastUpdatedBy() {
		return this.ctryLastUpdatedBy;
	}

	public void setCtryLastUpdatedBy(String ctryLastUpdatedBy) {
		this.ctryLastUpdatedBy = ctryLastUpdatedBy;
	}

	public String getCtryName() {
		return this.ctryName;
	}

	public void setCtryName(String ctryName) {
		this.ctryName = ctryName;
	}

	public int getCtryTimestamp() {
		return this.ctryTimestamp;
	}

	public void setCtryTimestamp(int ctryTimestamp) {
		this.ctryTimestamp = ctryTimestamp;
	}

}