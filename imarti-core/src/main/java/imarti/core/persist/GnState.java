package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_state database table.
 * 
 */
@Entity
@Table(name = "gn_state")
@NamedQuery(name = "GnState.findAll", query = "SELECT g FROM GnState g")
public class GnState implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_state")
	@GenericGenerator(name = "gn_state", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_state"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "stae_id") })
	@Column(name = "stae_id")
	private String staeId;

	@Column(name = "stae_code")
	private String staeCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "stae_last_updated")
	private Date staeLastUpdated;

	@Column(name = "stae_last_updated_by")
	private String staeLastUpdatedBy;

	@Column(name = "stae_name")
	private String staeName;

	@Column(name = "stae_timestamp")
	private int staeTimestamp;

	// bi-directional many-to-one association to GnCountry
	@ManyToOne
	@JoinColumn(name = "stae_country_id")
	private GnCountry gnCountry;

	public GnState() {
	}

	public String getStaeId() {
		return this.staeId;
	}

	public void setStaeId(String staeId) {
		this.staeId = staeId;
	}

	public String getStaeCode() {
		return this.staeCode;
	}

	public void setStaeCode(String staeCode) {
		this.staeCode = staeCode;
	}

	public Date getStaeLastUpdated() {
		return this.staeLastUpdated;
	}

	public void setStaeLastUpdated(Date staeLastUpdated) {
		this.staeLastUpdated = staeLastUpdated;
	}

	public String getStaeLastUpdatedBy() {
		return this.staeLastUpdatedBy;
	}

	public void setStaeLastUpdatedBy(String staeLastUpdatedBy) {
		this.staeLastUpdatedBy = staeLastUpdatedBy;
	}

	public String getStaeName() {
		return this.staeName;
	}

	public void setStaeName(String staeName) {
		this.staeName = staeName;
	}

	public int getStaeTimestamp() {
		return this.staeTimestamp;
	}

	public void setStaeTimestamp(int staeTimestamp) {
		this.staeTimestamp = staeTimestamp;
	}

	public GnCountry getGnCountry() {
		return this.gnCountry;
	}

	public void setGnCountry(GnCountry gnCountry) {
		this.gnCountry = gnCountry;
	}

}