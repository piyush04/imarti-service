package imarti.core.persist;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

/**
 * The persistent class for the gn_code_type database table.
 * 
 */
@Entity
@Table(name = "gn_code_type")
@NamedQuery(name = "GnCodeType.findAll", query = "SELECT g FROM GnCodeType g")
public class GnCodeType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_code_type")
	@GenericGenerator(name = "gn_code_type", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_code_type"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "cdty_id") })
	@Column(name = "cdty_id")
	private String cdtyId;

	@Column(name = "cdty_code")
	private String cdtyCode;

	@Column(name = "cdty_description")
	private String cdtyDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cdty_last_updated")
	private Date cdtyLastUpdated;

	@Column(name = "cdty_last_updated_by")
	private String cdtyLastUpdatedBy;

	@Column(name = "cdty_timestamp")
	private int cdtyTimestamp;

	public GnCodeType() {
	}

	public String getCdtyId() {
		return this.cdtyId;
	}

	public void setCdtyId(String cdtyId) {
		this.cdtyId = cdtyId;
	}

	public String getCdtyCode() {
		return this.cdtyCode;
	}

	public void setCdtyCode(String cdtyCode) {
		this.cdtyCode = cdtyCode;
	}

	public String getCdtyDescription() {
		return this.cdtyDescription;
	}

	public void setCdtyDescription(String cdtyDescription) {
		this.cdtyDescription = cdtyDescription;
	}

	public Date getCdtyLastUpdated() {
		return this.cdtyLastUpdated;
	}

	public void setCdtyLastUpdated(Date cdtyLastUpdated) {
		this.cdtyLastUpdated = cdtyLastUpdated;
	}

	public String getCdtyLastUpdatedBy() {
		return this.cdtyLastUpdatedBy;
	}

	public void setCdtyLastUpdatedBy(String cdtyLastUpdatedBy) {
		this.cdtyLastUpdatedBy = cdtyLastUpdatedBy;
	}

	public int getCdtyTimestamp() {
		return this.cdtyTimestamp;
	}

	public void setCdtyTimestamp(int cdtyTimestamp) {
		this.cdtyTimestamp = cdtyTimestamp;
	}

}