package imarti.core.persist;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;


/**
 * The persistent class for the gn_routine database table.
 * 
 */
@Entity
@Table(name="gn_routine")
@NamedQuery(name="GnRoutine.findAll", query="SELECT g FROM GnRoutine g")
public class GnRoutine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_routine")
	@GenericGenerator(name = "gn_routine", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_routine"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "rotn_id") })
	@Column(name="rotn_id")
	private String rotnId;

	public GnRoutine() {
	}

	public String getRotnId() {
		return this.rotnId;
	}

	public void setRotnId(String rotnId) {
		this.rotnId = rotnId;
	}

}