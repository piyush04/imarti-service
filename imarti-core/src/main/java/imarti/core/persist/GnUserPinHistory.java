package imarti.core.persist;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;

import java.util.Date;


/**
 * The persistent class for the gn_user_pin_history database table.
 * 
 */
@Entity
@Table(name="gn_user_pin_history")
@NamedQuery(name="GnUserPinHistory.findAll", query="SELECT g FROM GnUserPinHistory g")
public class GnUserPinHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_user_pin_history")
	@GenericGenerator(name = "gn_user_pin_history", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_user_pin_history"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "upht_id") })
	@Column(name="upht_id")
	private String uphtId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="upht_creation_date")
	private Date uphtCreationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="upht_last_updated")
	private Date uphtLastUpdated;

	@Column(name="upht_last_updated_by")
	private String uphtLastUpdatedBy;

	@Column(name="upht_pin_number")
	private String uphtPinNumber;

	@Column(name="upht_recipe")
	private String uphtRecipe;

	@Column(name="upht_registration_number")
	private String uphtRegistrationNumber;

	@Column(name="upht_salt")
	private String uphtSalt;

	@Column(name="upht_timestamp")
	private int uphtTimestamp;

	//bi-directional many-to-one association to GnUserPin
	@ManyToOne
	@JoinColumn(name="upht_user_pin_id")
	private GnUserPin gnUserPin;

	public GnUserPinHistory() {
	}

	public String getUphtId() {
		return this.uphtId;
	}

	public void setUphtId(String uphtId) {
		this.uphtId = uphtId;
	}

	public Date getUphtCreationDate() {
		return this.uphtCreationDate;
	}

	public void setUphtCreationDate(Date uphtCreationDate) {
		this.uphtCreationDate = uphtCreationDate;
	}

	public Date getUphtLastUpdated() {
		return this.uphtLastUpdated;
	}

	public void setUphtLastUpdated(Date uphtLastUpdated) {
		this.uphtLastUpdated = uphtLastUpdated;
	}

	public String getUphtLastUpdatedBy() {
		return this.uphtLastUpdatedBy;
	}

	public void setUphtLastUpdatedBy(String uphtLastUpdatedBy) {
		this.uphtLastUpdatedBy = uphtLastUpdatedBy;
	}

	public String getUphtPinNumber() {
		return this.uphtPinNumber;
	}

	public void setUphtPinNumber(String uphtPinNumber) {
		this.uphtPinNumber = uphtPinNumber;
	}

	public String getUphtRecipe() {
		return this.uphtRecipe;
	}

	public void setUphtRecipe(String uphtRecipe) {
		this.uphtRecipe = uphtRecipe;
	}

	public String getUphtRegistrationNumber() {
		return this.uphtRegistrationNumber;
	}

	public void setUphtRegistrationNumber(String uphtRegistrationNumber) {
		this.uphtRegistrationNumber = uphtRegistrationNumber;
	}

	public String getUphtSalt() {
		return this.uphtSalt;
	}

	public void setUphtSalt(String uphtSalt) {
		this.uphtSalt = uphtSalt;
	}

	public int getUphtTimestamp() {
		return this.uphtTimestamp;
	}

	public void setUphtTimestamp(int uphtTimestamp) {
		this.uphtTimestamp = uphtTimestamp;
	}

	public GnUserPin getGnUserPin() {
		return this.gnUserPin;
	}

	public void setGnUserPin(GnUserPin gnUserPin) {
		this.gnUserPin = gnUserPin;
	}

}