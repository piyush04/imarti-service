package imarti.core.persist;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import imarti.core.event.hibernate.tool.HibernateGenerator;


/**
 * The persistent class for the gn_orders database table.
 * 
 */
@Entity
@Table(name="gn_orders")
@NamedQuery(name="GnOrder.findAll", query="SELECT g FROM GnOrder g")
public class GnOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gn_orders")
	@GenericGenerator(name = "gn_orders", strategy = "imarti.core.event.hibernate.tool.HibernateGenerator", parameters = {
			@Parameter(name = HibernateGenerator.TABLE_NAME, value = "gn_orders"),
			@Parameter(name = HibernateGenerator.PRMIARY_COLUMN_NAME, value = "ordr_id") })
	@Column(name="ordr_id")
	private String ordrId;

	public GnOrder() {
	}

	public String getOrdrId() {
		return this.ordrId;
	}

	public void setOrdrId(String ordrId) {
		this.ordrId = ordrId;
	}

}