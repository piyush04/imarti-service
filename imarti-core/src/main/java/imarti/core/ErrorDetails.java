package imarti.core;

import java.io.Serializable;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author AbhinitKumar
 *
 */
@JsonInclude(Include.NON_NULL)
public final class ErrorDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private Instant timestamp;
	private String type;
	private String code;
	private String message;

	public ErrorDetails(String type, String code, String message) {
		super();
		this.timestamp = Instant.now();
		this.code = code;
		this.message = message;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
