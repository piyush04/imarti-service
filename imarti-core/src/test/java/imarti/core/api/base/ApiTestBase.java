package imarti.core.api.base;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;

import java.util.Map;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.StandaloneMockMvcBuilder;
import org.springframework.validation.annotation.Validated;

import imarti.core.exception.ResponseException;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.module.mockmvc.response.MockMvcResponse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Validated
public abstract class ApiTestBase {

	protected void init(Object... apiObject) {
		StandaloneMockMvcBuilder standaloneMockMvcBuilder = MockMvcBuilders.standaloneSetup(apiObject)
				.setControllerAdvice(ResponseException.class);
		RestAssuredMockMvc.standaloneSetup(standaloneMockMvcBuilder);

	}

	protected MockMvcResponse postRequest(String fullURL, String jsonBody) {
		return given().accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.body(jsonBody).post(fullURL);

	}

	protected MockMvcResponse postRequest(String fullURL, String jsonBody, Map<String, String> params) {
		return given().accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.params(params).body(jsonBody).post(fullURL);

	}

	protected MockMvcResponse postRequest(String fullURL, String jsonBody, Map<String, String> params,
			Map<String, String> headerParams) {
		return given().accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.headers(headerParams).params(params).body(jsonBody).post(fullURL);

	}

	protected MockMvcResponse postRequest(Map<String, String> headerParams, String fullURL, String jsonBody) {
		return given().accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.headers(headerParams).body(jsonBody).post(fullURL);

	}

	protected MockMvcResponse getRequest(String fullURL, Map<String, String> params) {
		return given().accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.params(params).get(fullURL);

	}

	protected MockMvcResponse deleteRequest(String fullURL, Map<String, String> params) {
		return given().accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
				.params(params).delete(fullURL);
	}

}
