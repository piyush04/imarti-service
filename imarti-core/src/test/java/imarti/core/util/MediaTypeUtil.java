package imarti.core.util;

/**
 * @author AbhinitKumar
 *
 */
public final class MediaTypeUtil {

	private MediaTypeUtil() {
		super();
	}

	public static final String CONTENT_TYPE = "Content-Type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String ACCEPT = "accept";

}
