package imarti.microservices.payment.message;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import imarti.core.message.FaultCode;

/**
 * @author AbhinitKumar
 *
 */
public enum PaymentFaultCode implements FaultCode {

	FNCE_C0001, FNCE_C0002, FNCE_C0003, FNCE_C0004, FNCE_C0005, FNCE_C0006, FNCE_C0007, FNCE_C0008, FNCE_C0009,
	FNCE_C0010, FNCE_C0012, FNCE_C0013, FNCE_C0014, FNCE_C0015, FNCE_C0016, FNCE_C0017, FNCE_C0018, FNCE_C0019,
	FNCE_C0020, FNCE_C0021, FNCE_C0022, FNCE_C0023, FNCE_C0024, FNCE_C0025, FNCE_C0026, FNCE_C0027, FNCE_C0028,
	FNCE_C0029;

	private static final String PATH = "classpath:i18n/messages";
	private static ReloadableResourceBundleMessageSource resource;
	private static final Logger logger = LogManager.getLogger(PaymentFaultCode.class);

	@Autowired
	MessageSource bundleMessageSource;

	public String getKey() {
		return toString();
	}

	@Override
	public ReloadableResourceBundleMessageSource getBundle() {
		try {
			logger.info("Start loading property file.");
			resource = new ReloadableResourceBundleMessageSource();
			resource.setBasename(PATH);
			resource.setDefaultEncoding("UTF-8");
			logger.info("Property file loaded.");

		} catch (Exception e) {
			logger.debug("Error in loading roperty file", e);
			logger.error("Start loading property file.", e);
		}
		return resource;
	}

}
