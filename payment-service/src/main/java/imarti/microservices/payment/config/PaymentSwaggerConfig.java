package imarti.microservices.payment.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@PropertySource(value = { "classpath:i18n/swagger/swaggerMessages.properties" })
public class PaymentSwaggerConfig {

	@Autowired
	private Environment environment;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("imarti.microservices.finance.api"))
				.paths(PathSelectors.ant("/cs/**")).build().apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(environment.getRequiredProperty("swagger.title"))
				.description(environment.getRequiredProperty("swagger.description"))
				.version(environment.getRequiredProperty("swagger.version"))
				// .contact(new Contact(environment.getRequiredProperty("swagger.contact.name"),
				// environment.getRequiredProperty("swagger.contact.url"),
				// environment.getRequiredProperty("swagger.contact.email")))
				// .license("swagger.license")
				.build();
	}
}
