package imarti.microservices.common.message;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import imarti.core.message.FaultCode;

/**
 * @author AbhinitKumar
 *
 */
public enum CommonFaultCode implements FaultCode {

	CMNE_C0001, CMNE_C0002, CMNE_C0003, CMNE_C0004, CMNE_C0005, CMNE_C0006, CMNE_C0007, CMNE_C0008, CMNE_C0009,
	CMNE_C0010,;

	private static final String PATH = "classpath:i18n/error/errorMessages";
	private static ReloadableResourceBundleMessageSource resource;
	private static final Logger logger = LogManager.getLogger(CommonFaultCode.class);

	@Autowired
	MessageSource bundleMessageSource;

	public String getKey() {
		return toString();
	}

	@Override
	public ReloadableResourceBundleMessageSource getBundle() {
		try {
			logger.info("Start loading property file.");
			resource = new ReloadableResourceBundleMessageSource();
			resource.setBasename(PATH);
			resource.setDefaultEncoding("UTF-8");
			logger.info("Property file loaded.");

		} catch (Exception e) {
			logger.debug("Error in loading roperty file", e);
			logger.error("Start loading property file.", e);
		}
		return resource;
	}

}
