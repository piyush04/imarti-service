package imarti.microservices.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "imarti.microservices.common" })
public class CommonAppConfig {

	


}
