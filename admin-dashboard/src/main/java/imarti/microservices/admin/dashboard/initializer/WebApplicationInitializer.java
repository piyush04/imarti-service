package imarti.microservices.admin.dashboard.initializer;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import imarti.microservices.admin.dashboard.config.AppConfig;
import imarti.microservices.admin.dashboard.config.HazelcastConfig;
import imarti.microservices.admin.dashboard.config.NotifierConfiguration;
import imarti.microservices.admin.dashboard.config.WebSecurityConfig;

/**
 * @author AbhinitKumar
 *
 */
public class WebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { AppConfig.class, WebSecurityConfig.class, NotifierConfiguration.class,
				HazelcastConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
