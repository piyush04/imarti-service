package imarti.microservices.admin.dashboard.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("imarti.microservices.admin.dashboard")
public class AppConfig {

}
