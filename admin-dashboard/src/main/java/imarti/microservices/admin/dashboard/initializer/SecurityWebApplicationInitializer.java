package imarti.microservices.admin.dashboard.initializer;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author AbhinitKumar
 *
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}