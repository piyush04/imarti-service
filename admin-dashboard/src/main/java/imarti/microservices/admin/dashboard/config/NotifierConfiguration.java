package imarti.microservices.admin.dashboard.config;

import java.time.Duration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;

import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.notify.LoggingNotifier;
import de.codecentric.boot.admin.server.notify.RemindingNotifier;
import de.codecentric.boot.admin.server.notify.filter.FilteringNotifier;

@Configuration	
public class NotifierConfiguration {
	
	 private final InstanceRepository repository;

     public NotifierConfiguration(InstanceRepository repository) {
         this.repository = repository;
     }

     @Bean
     @Primary
     public RemindingNotifier remindingNotifier() {
         RemindingNotifier notifier = new RemindingNotifier(filteringNotifier(), repository);
         notifier.setReminderPeriod(Duration.ofMinutes(10));
         return notifier;
     }

     @Scheduled(fixedRate = 1_000L)
     public void remind() {
         remindingNotifier().setReminderPeriod(Duration.ofMinutes(5));
     }

     @Bean
     public FilteringNotifier filteringNotifier() {
         return new FilteringNotifier(loggerNotifier(), repository);
     }

     @Bean
     public LoggingNotifier loggerNotifier() {
         return new LoggingNotifier(repository);
     }
}